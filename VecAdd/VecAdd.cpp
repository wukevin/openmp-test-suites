
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

// Define a kernel function:
// The function must be defined first and declared.
void vector_add(float *a, float *b, float *c, const int N) {
  int i = omp_get_num_threads();
  c[i] = a[i] + b[i];
}
#pragma omp declare target
void vector_add(float *a, float *b, float *c, const int N);
#pragma omp end declare target

int main (int argc, char *argv[])
{

  const int N = 1024;

  float *a = (float*)malloc(N*sizeof(float));
  float *b = (float*)malloc(N*sizeof(float));
  float *c = (float*)malloc(N*sizeof(float));

  for(int i = 0; i < N; ++i) {
	a[i] = i;
	b[i] = i;
  }

  #pragma omp target map(to:a[0:N],b[0:N],N) map(from: c[0:N])
  {
       vector_add(a, b, c, N);
  }
  #pragma omp target update from(c[0:N])

  int err = 1;

  for(int i = 0; i < N; ++i) {
	err &= (c[i] == (a[i] + b[i]));
  }

  free(a);
  free(b);
  free(c);

  return !err;
}

