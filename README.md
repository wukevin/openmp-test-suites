# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is used for OpenMP-4.x testing, which includes: 
    * Testing with examples from openmp.org (v4.0.2)
    * OpenMP regression tests from LLVM/CLANG 3.9 test suite 
    * OpenMP Validation Suite by HLRS, Univ. Stuttgart and Univ. of Houston 
    * OpenMP task test suite by BSC 
    * EPCC Microbenchmarks
    * NAS Benchmarks  NPB 3.3.1 

### How do I get set up? ###

### OpenMP-4.0.2
		clang trunk reported 17 failures out of 35 tests
		Steps:
			1. export LLVM_PATH=/WHERE/YOU/BUILD/LLVM+CLANG
			2. ./run_tests.sh
    or
			2. cd ${SUBDIRECTORY}
			3. make
### clang-test
		clang-trunk reported 3 failures out of 326 tests
		CMake version 3.0.2 or up is required
		Steps:
			1. mkdir build
			2. cd build
			3. cmake ../ -DLLVM_PATH=/WHERE/YOU/BUILD/LLVM+CLANG
			4. make test
			5. The results of testing will be stored in build/Testing/Temporary
### clang-openmp-project
		clang-trunk reports no error compilation
		The execution results encountered 5 failures out of 123 tests
		Steps:
			1. export LLVM_PATH=/WHERE/YOU/BUILD/LLVM+CLANG
			2. cd testsuite
			3. make ctest
### HLRS
		not avaliable now
### BOTS-1.1.2
		clang-trunk reports no error compilation
		Steps:
			1. export LLVM_PATH=/WHERE/YOU/BUILD/LLVM+CLANG
			2. ./configure
			3. select clang as our CC
			4. make
			   The executables will be generated in bin
			5. ./run_tests.sh
### EPCC-3.1
		clang-trunk reports no error compilation
		Steps:
			1. export LLVM_PATH=/WHERE/YOU/BUILD/LLVM+CLANG
			2. make
 			3. ./run_tests.sh
### NBP-3.3.1-OMP
		clang-trunk reports no error compilation
		Steps:
			1. export LLVM_PATH=/WHERE/YOU/BUILD/LLVM+CLANG
			2. mkdir bin
			3. make suite
			   The executables will be generated in bin
			4. ./run_tests.sh

*** For more compilation details of each test suite, please refer to each README in subdirectories ***
	

### Who do I talk to? ###

* Repo owner or admin
    * andrew@multicorewareinc.com
* Other community or team contact
