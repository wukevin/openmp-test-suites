; ModuleID = 'atomic_update_codegen.cpp'
source_filename = "atomic_update_codegen.cpp"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%ident_t = type { i32, i32, i32, i32, i8* }
%struct.BitFields = type { i64 }
%struct.BitFields_packed = type { i64 }
%struct.BitFields2 = type { i32 }
%struct.BitFields2_packed = type { i32 }
%struct.BitFields3 = type { i32 }
%struct.BitFields3_packed = type { i32 }
%struct.BitFields4 = type { i24, [4 x i8] }
%struct.BitFields4_packed = type { [3 x i8] }

$__clang_call_terminate = comdat any

@dv = global double 0.000000e+00, align 8
@bx = global i8 0, align 1
@cx = global i8 0, align 1
@ucx = global i8 0, align 1
@sx = global i16 0, align 2
@usx = global i16 0, align 2
@usv = global i16 0, align 2
@ix = global i32 0, align 4
@iv = global i32 0, align 4
@uix = global i32 0, align 4
@uiv = global i32 0, align 4
@lx = global i64 0, align 8
@lv = global i64 0, align 8
@ulx = global i64 0, align 8
@ulv = global i64 0, align 8
@llx = global i64 0, align 8
@llv = global i64 0, align 8
@ullx = global i64 0, align 8
@ullv = global i64 0, align 8
@fx = global float 0.000000e+00, align 4
@fv = global float 0.000000e+00, align 4
@dx = global double 0.000000e+00, align 8
@ldx = global x86_fp80 0xK00000000000000000000, align 16
@ldv = global x86_fp80 0xK00000000000000000000, align 16
@cix = global { i32, i32 } zeroinitializer, align 4
@civ = global { i32, i32 } zeroinitializer, align 4
@cfx = global { float, float } zeroinitializer, align 4
@cfv = global { float, float } zeroinitializer, align 4
@cdx = global { double, double } zeroinitializer, align 8
@cdv = global { double, double } zeroinitializer, align 8
@.str = private unnamed_addr constant [23 x i8] c";unknown;unknown;0;0;;\00", align 1
@0 = private unnamed_addr constant %ident_t { i32 0, i32 2, i32 0, i32 0, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i32 0, i32 0) }, align 8
@bv = global i8 0, align 1
@cv = global i8 0, align 1
@ucv = global i8 0, align 1
@sv = global i16 0, align 2
@int4x = global <4 x i32> zeroinitializer, align 16
@bfx = global %struct.BitFields zeroinitializer, align 4
@bfx_packed = global %struct.BitFields_packed zeroinitializer, align 1
@bfx2 = global %struct.BitFields2 zeroinitializer, align 4
@bfx2_packed = global %struct.BitFields2_packed zeroinitializer, align 1
@bfx3 = global %struct.BitFields3 zeroinitializer, align 4
@bfx3_packed = global %struct.BitFields3_packed zeroinitializer, align 1
@bfx4 = global %struct.BitFields4 zeroinitializer, align 8
@bfx4_packed = global %struct.BitFields4_packed zeroinitializer, align 1
@float2x = global <2 x float> zeroinitializer, align 8

; Function Attrs: norecurse uwtable
define i32 @main() #0 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %retval = alloca i32, align 4
  %atomic-temp = alloca double, align 8
  %atomic-temp3 = alloca i16, align 2
  %atomic-temp10 = alloca i32, align 4
  %atomic-temp14 = alloca i32, align 4
  %atomic-temp18 = alloca i32, align 4
  %atomic-temp22 = alloca i64, align 8
  %atomic-temp26 = alloca float, align 4
  %atomic-temp31 = alloca double, align 8
  %atomic-temp35 = alloca x86_fp80, align 16
  %atomic-temp36 = alloca x86_fp80, align 16
  %atomic-temp39 = alloca { i32, i32 }, align 4
  %atomic-temp41 = alloca { i32, i32 }, align 4
  %atomic-temp44 = alloca { float, float }, align 4
  %atomic-temp47 = alloca { float, float }, align 4
  %atomic-temp51 = alloca { double, double }, align 8
  %atomic-temp54 = alloca { double, double }, align 8
  %atomic-temp62 = alloca i8, align 1
  %atomic-temp70 = alloca i8, align 1
  %atomic-temp78 = alloca i64, align 8
  %atomic-temp85 = alloca i64, align 8
  %atomic-temp87 = alloca { i32, i32 }, align 4
  %atomic-temp90 = alloca { i32, i32 }, align 4
  %atomic-temp103 = alloca float, align 4
  %atomic-temp109 = alloca double, align 8
  %atomic-temp115 = alloca x86_fp80, align 16
  %atomic-temp116 = alloca x86_fp80, align 16
  %atomic-temp119 = alloca { i32, i32 }, align 4
  %atomic-temp122 = alloca { i32, i32 }, align 4
  %coerce = alloca { float, float }, align 4
  %atomic-temp133 = alloca i16, align 2
  %atomic-temp141 = alloca i8, align 1
  %atomic-temp153 = alloca i8, align 1
  %atomic-temp167 = alloca i16, align 2
  %coerce171 = alloca { float, float }, align 4
  %atomic-temp178 = alloca i64, align 8
  %atomic-temp188 = alloca <4 x i32>, align 16
  %atomic-temp189 = alloca <4 x i32>, align 16
  %atomic-temp193 = alloca i32, align 4
  %atomic-temp194 = alloca i32, align 4
  %atomic-temp200 = alloca i32, align 1
  %atomic-temp203 = alloca i32, align 1
  %atomic-temp204 = alloca i32, align 1
  %atomic-temp220 = alloca i32, align 4
  %atomic-temp221 = alloca i32, align 4
  %atomic-temp235 = alloca i32, align 1
  %atomic-temp236 = alloca i32, align 1
  %atomic-temp250 = alloca i32, align 4
  %atomic-temp251 = alloca i32, align 4
  %atomic-temp264 = alloca i32, align 1
  %atomic-temp267 = alloca i32, align 1
  %atomic-temp268 = alloca i32, align 1
  %atomic-temp286 = alloca i64, align 8
  %atomic-temp287 = alloca i64, align 8
  %atomic-temp303 = alloca i32, align 1
  %atomic-temp304 = alloca i32, align 1
  %atomic-temp319 = alloca i64, align 8
  %atomic-temp320 = alloca i64, align 8
  %atomic-temp335 = alloca i64, align 1
  %atomic-temp336 = alloca i64, align 1
  %atomic-temp352 = alloca <2 x float>, align 8
  %atomic-temp353 = alloca <2 x float>, align 8
  store i32 0, i32* %retval, align 4
  %atomic-load = load atomic i64, i64* bitcast (double* @dv to i64*) monotonic, align 8
  br label %atomic_cont

atomic_cont:                                      ; preds = %atomic_cont, %entry
  %0 = phi i64 [ %atomic-load, %entry ], [ %5, %atomic_cont ]
  %1 = bitcast double* %atomic-temp to i64*
  %2 = bitcast i64 %0 to double
  %add = fadd double %2, 1.000000e+00
  store double %add, double* %atomic-temp, align 8
  %3 = load i64, i64* %1, align 8
  %4 = cmpxchg i64* bitcast (double* @dv to i64*), i64 %0, i64 %3 monotonic monotonic
  %5 = extractvalue { i64, i1 } %4, 0
  %6 = extractvalue { i64, i1 } %4, 1
  br i1 %6, label %atomic_exit, label %atomic_cont

atomic_exit:                                      ; preds = %atomic_cont
  %7 = atomicrmw add i8* @bx, i8 1 monotonic
  %8 = atomicrmw add i8* @cx, i8 1 monotonic
  %9 = atomicrmw sub i8* @ucx, i8 1 monotonic
  %10 = atomicrmw sub i16* @sx, i16 1 monotonic
  %11 = load i16, i16* @usv, align 2
  %conv = zext i16 %11 to i32
  %atomic-load1 = load atomic i16, i16* @usx monotonic, align 2
  br label %atomic_cont2

atomic_cont2:                                     ; preds = %atomic_cont2, %atomic_exit
  %12 = phi i16 [ %atomic-load1, %atomic_exit ], [ %15, %atomic_cont2 ]
  %conv4 = zext i16 %12 to i32
  %add5 = add nsw i32 %conv4, %conv
  %conv6 = trunc i32 %add5 to i16
  store i16 %conv6, i16* %atomic-temp3, align 2
  %13 = load i16, i16* %atomic-temp3, align 2
  %14 = cmpxchg i16* @usx, i16 %12, i16 %13 monotonic monotonic
  %15 = extractvalue { i16, i1 } %14, 0
  %16 = extractvalue { i16, i1 } %14, 1
  br i1 %16, label %atomic_exit7, label %atomic_cont2

atomic_exit7:                                     ; preds = %atomic_cont2
  %17 = load i32, i32* @iv, align 4
  %atomic-load8 = load atomic i32, i32* @ix monotonic, align 4
  br label %atomic_cont9

atomic_cont9:                                     ; preds = %atomic_cont9, %atomic_exit7
  %18 = phi i32 [ %atomic-load8, %atomic_exit7 ], [ %21, %atomic_cont9 ]
  %mul = mul nsw i32 %18, %17
  store i32 %mul, i32* %atomic-temp10, align 4
  %19 = load i32, i32* %atomic-temp10, align 4
  %20 = cmpxchg i32* @ix, i32 %18, i32 %19 monotonic monotonic
  %21 = extractvalue { i32, i1 } %20, 0
  %22 = extractvalue { i32, i1 } %20, 1
  br i1 %22, label %atomic_exit11, label %atomic_cont9

atomic_exit11:                                    ; preds = %atomic_cont9
  %23 = load i32, i32* @uiv, align 4
  %24 = atomicrmw sub i32* @uix, i32 %23 monotonic
  %25 = load i32, i32* @iv, align 4
  %atomic-load12 = load atomic i32, i32* @ix monotonic, align 4
  br label %atomic_cont13

atomic_cont13:                                    ; preds = %atomic_cont13, %atomic_exit11
  %26 = phi i32 [ %atomic-load12, %atomic_exit11 ], [ %29, %atomic_cont13 ]
  %shl = shl i32 %26, %25
  store i32 %shl, i32* %atomic-temp14, align 4
  %27 = load i32, i32* %atomic-temp14, align 4
  %28 = cmpxchg i32* @ix, i32 %26, i32 %27 monotonic monotonic
  %29 = extractvalue { i32, i1 } %28, 0
  %30 = extractvalue { i32, i1 } %28, 1
  br i1 %30, label %atomic_exit15, label %atomic_cont13

atomic_exit15:                                    ; preds = %atomic_cont13
  %31 = load i32, i32* @uiv, align 4
  %atomic-load16 = load atomic i32, i32* @uix monotonic, align 4
  br label %atomic_cont17

atomic_cont17:                                    ; preds = %atomic_cont17, %atomic_exit15
  %32 = phi i32 [ %atomic-load16, %atomic_exit15 ], [ %35, %atomic_cont17 ]
  %shr = lshr i32 %32, %31
  store i32 %shr, i32* %atomic-temp18, align 4
  %33 = load i32, i32* %atomic-temp18, align 4
  %34 = cmpxchg i32* @uix, i32 %32, i32 %33 monotonic monotonic
  %35 = extractvalue { i32, i1 } %34, 0
  %36 = extractvalue { i32, i1 } %34, 1
  br i1 %36, label %atomic_exit19, label %atomic_cont17

atomic_exit19:                                    ; preds = %atomic_cont17
  %37 = load i64, i64* @lv, align 8
  %atomic-load20 = load atomic i64, i64* @lx monotonic, align 8
  br label %atomic_cont21

atomic_cont21:                                    ; preds = %atomic_cont21, %atomic_exit19
  %38 = phi i64 [ %atomic-load20, %atomic_exit19 ], [ %41, %atomic_cont21 ]
  %div = sdiv i64 %38, %37
  store i64 %div, i64* %atomic-temp22, align 8
  %39 = load i64, i64* %atomic-temp22, align 8
  %40 = cmpxchg i64* @lx, i64 %38, i64 %39 monotonic monotonic
  %41 = extractvalue { i64, i1 } %40, 0
  %42 = extractvalue { i64, i1 } %40, 1
  br i1 %42, label %atomic_exit23, label %atomic_cont21

atomic_exit23:                                    ; preds = %atomic_cont21
  %43 = load i64, i64* @ulv, align 8
  %44 = atomicrmw and i64* @ulx, i64 %43 monotonic
  %45 = load i64, i64* @llv, align 8
  %46 = atomicrmw xor i64* @llx, i64 %45 monotonic
  %47 = load i64, i64* @ullv, align 8
  %48 = atomicrmw or i64* @ullx, i64 %47 monotonic
  %49 = load float, float* @fv, align 4
  %atomic-load24 = load atomic i32, i32* bitcast (float* @fx to i32*) monotonic, align 4
  br label %atomic_cont25

atomic_cont25:                                    ; preds = %atomic_cont25, %atomic_exit23
  %50 = phi i32 [ %atomic-load24, %atomic_exit23 ], [ %55, %atomic_cont25 ]
  %51 = bitcast float* %atomic-temp26 to i32*
  %52 = bitcast i32 %50 to float
  %add27 = fadd float %52, %49
  store float %add27, float* %atomic-temp26, align 4
  %53 = load i32, i32* %51, align 4
  %54 = cmpxchg i32* bitcast (float* @fx to i32*), i32 %50, i32 %53 monotonic monotonic
  %55 = extractvalue { i32, i1 } %54, 0
  %56 = extractvalue { i32, i1 } %54, 1
  br i1 %56, label %atomic_exit28, label %atomic_cont25

atomic_exit28:                                    ; preds = %atomic_cont25
  %57 = load double, double* @dv, align 8
  %atomic-load29 = load atomic i64, i64* bitcast (double* @dx to i64*) monotonic, align 8
  br label %atomic_cont30

atomic_cont30:                                    ; preds = %atomic_cont30, %atomic_exit28
  %58 = phi i64 [ %atomic-load29, %atomic_exit28 ], [ %63, %atomic_cont30 ]
  %59 = bitcast double* %atomic-temp31 to i64*
  %60 = bitcast i64 %58 to double
  %sub = fsub double %57, %60
  store double %sub, double* %atomic-temp31, align 8
  %61 = load i64, i64* %59, align 8
  %62 = cmpxchg i64* bitcast (double* @dx to i64*), i64 %58, i64 %61 monotonic monotonic
  %63 = extractvalue { i64, i1 } %62, 0
  %64 = extractvalue { i64, i1 } %62, 1
  br i1 %64, label %atomic_exit32, label %atomic_cont30

atomic_exit32:                                    ; preds = %atomic_cont30
  %65 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load33 = load atomic i128, i128* bitcast (x86_fp80* @ldx to i128*) monotonic, align 16
  br label %atomic_cont34

atomic_cont34:                                    ; preds = %atomic_cont34, %atomic_exit32
  %66 = phi i128 [ %atomic-load33, %atomic_exit32 ], [ %72, %atomic_cont34 ]
  %67 = bitcast x86_fp80* %atomic-temp35 to i128*
  store i128 %66, i128* %67, align 16
  %68 = bitcast x86_fp80* %atomic-temp36 to i128*
  store i128 %66, i128* %68, align 16
  %69 = load x86_fp80, x86_fp80* %atomic-temp36, align 16
  %mul37 = fmul x86_fp80 %69, %65
  store x86_fp80 %mul37, x86_fp80* %atomic-temp35, align 16
  %70 = load i128, i128* %67, align 16
  %71 = cmpxchg i128* bitcast (x86_fp80* @ldx to i128*), i128 %66, i128 %70 monotonic monotonic
  %72 = extractvalue { i128, i1 } %71, 0
  %73 = extractvalue { i128, i1 } %71, 1
  br i1 %73, label %atomic_exit38, label %atomic_cont34

atomic_exit38:                                    ; preds = %atomic_cont34
  %civ.real = load i32, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 0), align 4
  %civ.imag = load i32, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 1), align 4
  %74 = bitcast { i32, i32 }* %atomic-temp39 to i8*
  invoke void @__atomic_load(i64 8, i8* bitcast ({ i32, i32 }* @cix to i8*), i8* %74, i32 0)
          to label %invoke.cont unwind label %terminate.lpad

invoke.cont:                                      ; preds = %atomic_exit38
  br label %atomic_cont40

atomic_cont40:                                    ; preds = %invoke.cont42, %invoke.cont
  %atomic-temp39.realp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp39, i32 0, i32 0
  %atomic-temp39.real = load i32, i32* %atomic-temp39.realp, align 4
  %atomic-temp39.imagp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp39, i32 0, i32 1
  %atomic-temp39.imag = load i32, i32* %atomic-temp39.imagp, align 4
  %75 = mul i32 %civ.real, %atomic-temp39.real
  %76 = mul i32 %civ.imag, %atomic-temp39.imag
  %77 = add i32 %75, %76
  %78 = mul i32 %atomic-temp39.real, %atomic-temp39.real
  %79 = mul i32 %atomic-temp39.imag, %atomic-temp39.imag
  %80 = add i32 %78, %79
  %81 = mul i32 %civ.imag, %atomic-temp39.real
  %82 = mul i32 %civ.real, %atomic-temp39.imag
  %83 = sub i32 %81, %82
  %84 = sdiv i32 %77, %80
  %85 = sdiv i32 %83, %80
  %atomic-temp41.realp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp41, i32 0, i32 0
  %atomic-temp41.imagp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp41, i32 0, i32 1
  store i32 %84, i32* %atomic-temp41.realp, align 4
  store i32 %85, i32* %atomic-temp41.imagp, align 4
  %86 = bitcast { i32, i32 }* %atomic-temp39 to i8*
  %87 = bitcast { i32, i32 }* %atomic-temp41 to i8*
  %call = invoke zeroext i1 @__atomic_compare_exchange(i64 8, i8* bitcast ({ i32, i32 }* @cix to i8*), i8* %86, i8* %87, i32 0, i32 0)
          to label %invoke.cont42 unwind label %terminate.lpad

invoke.cont42:                                    ; preds = %atomic_cont40
  br i1 %call, label %atomic_exit43, label %atomic_cont40

atomic_exit43:                                    ; preds = %invoke.cont42
  %cfv.real = load float, float* getelementptr inbounds ({ float, float }, { float, float }* @cfv, i32 0, i32 0), align 4
  %cfv.imag = load float, float* getelementptr inbounds ({ float, float }, { float, float }* @cfv, i32 0, i32 1), align 4
  %88 = bitcast { float, float }* %atomic-temp44 to i8*
  invoke void @__atomic_load(i64 8, i8* bitcast ({ float, float }* @cfx to i8*), i8* %88, i32 0)
          to label %invoke.cont45 unwind label %terminate.lpad

invoke.cont45:                                    ; preds = %atomic_exit43
  br label %atomic_cont46

atomic_cont46:                                    ; preds = %invoke.cont48, %invoke.cont45
  %atomic-temp44.realp = getelementptr inbounds { float, float }, { float, float }* %atomic-temp44, i32 0, i32 0
  %atomic-temp44.real = load float, float* %atomic-temp44.realp, align 4
  %atomic-temp44.imagp = getelementptr inbounds { float, float }, { float, float }* %atomic-temp44, i32 0, i32 1
  %atomic-temp44.imag = load float, float* %atomic-temp44.imagp, align 4
  %add.r = fadd float %cfv.real, %atomic-temp44.real
  %add.i = fadd float %cfv.imag, %atomic-temp44.imag
  %atomic-temp47.realp = getelementptr inbounds { float, float }, { float, float }* %atomic-temp47, i32 0, i32 0
  %atomic-temp47.imagp = getelementptr inbounds { float, float }, { float, float }* %atomic-temp47, i32 0, i32 1
  store float %add.r, float* %atomic-temp47.realp, align 4
  store float %add.i, float* %atomic-temp47.imagp, align 4
  %89 = bitcast { float, float }* %atomic-temp44 to i8*
  %90 = bitcast { float, float }* %atomic-temp47 to i8*
  %call49 = invoke zeroext i1 @__atomic_compare_exchange(i64 8, i8* bitcast ({ float, float }* @cfx to i8*), i8* %89, i8* %90, i32 0, i32 0)
          to label %invoke.cont48 unwind label %terminate.lpad

invoke.cont48:                                    ; preds = %atomic_cont46
  br i1 %call49, label %atomic_exit50, label %atomic_cont46

atomic_exit50:                                    ; preds = %invoke.cont48
  %cdv.real = load double, double* getelementptr inbounds ({ double, double }, { double, double }* @cdv, i32 0, i32 0), align 8
  %cdv.imag = load double, double* getelementptr inbounds ({ double, double }, { double, double }* @cdv, i32 0, i32 1), align 8
  %91 = bitcast { double, double }* %atomic-temp51 to i8*
  invoke void @__atomic_load(i64 16, i8* bitcast ({ double, double }* @cdx to i8*), i8* %91, i32 5)
          to label %invoke.cont52 unwind label %terminate.lpad

invoke.cont52:                                    ; preds = %atomic_exit50
  br label %atomic_cont53

atomic_cont53:                                    ; preds = %invoke.cont55, %invoke.cont52
  %atomic-temp51.realp = getelementptr inbounds { double, double }, { double, double }* %atomic-temp51, i32 0, i32 0
  %atomic-temp51.real = load double, double* %atomic-temp51.realp, align 8
  %atomic-temp51.imagp = getelementptr inbounds { double, double }, { double, double }* %atomic-temp51, i32 0, i32 1
  %atomic-temp51.imag = load double, double* %atomic-temp51.imagp, align 8
  %sub.r = fsub double %atomic-temp51.real, %cdv.real
  %sub.i = fsub double %atomic-temp51.imag, %cdv.imag
  %atomic-temp54.realp = getelementptr inbounds { double, double }, { double, double }* %atomic-temp54, i32 0, i32 0
  %atomic-temp54.imagp = getelementptr inbounds { double, double }, { double, double }* %atomic-temp54, i32 0, i32 1
  store double %sub.r, double* %atomic-temp54.realp, align 8
  store double %sub.i, double* %atomic-temp54.imagp, align 8
  %92 = bitcast { double, double }* %atomic-temp51 to i8*
  %93 = bitcast { double, double }* %atomic-temp54 to i8*
  %call56 = invoke zeroext i1 @__atomic_compare_exchange(i64 16, i8* bitcast ({ double, double }* @cdx to i8*), i8* %92, i8* %93, i32 5, i32 5)
          to label %invoke.cont55 unwind label %terminate.lpad

invoke.cont55:                                    ; preds = %atomic_cont53
  br i1 %call56, label %atomic_exit57, label %atomic_cont53

atomic_exit57:                                    ; preds = %invoke.cont55
  call void @__kmpc_flush(%ident_t* @0)
  %94 = load i8, i8* @bv, align 1
  %tobool = trunc i8 %94 to i1
  %conv58 = zext i1 %tobool to i64
  %95 = atomicrmw and i64* @ulx, i64 %conv58 monotonic
  %96 = load i8, i8* @cv, align 1
  %conv59 = sext i8 %96 to i32
  %atomic-load60 = load atomic i8, i8* @bx monotonic, align 1
  br label %atomic_cont61

atomic_cont61:                                    ; preds = %atomic_cont61, %atomic_exit57
  %97 = phi i8 [ %atomic-load60, %atomic_exit57 ], [ %100, %atomic_cont61 ]
  %tobool63 = trunc i8 %97 to i1
  %conv64 = zext i1 %tobool63 to i32
  %and = and i32 %conv59, %conv64
  %tobool65 = icmp ne i32 %and, 0
  %frombool = zext i1 %tobool65 to i8
  store i8 %frombool, i8* %atomic-temp62, align 1
  %98 = load i8, i8* %atomic-temp62, align 1
  %99 = cmpxchg i8* @bx, i8 %97, i8 %98 monotonic monotonic
  %100 = extractvalue { i8, i1 } %99, 0
  %101 = extractvalue { i8, i1 } %99, 1
  br i1 %101, label %atomic_exit66, label %atomic_cont61

atomic_exit66:                                    ; preds = %atomic_cont61
  %102 = load i8, i8* @ucv, align 1
  %conv67 = zext i8 %102 to i32
  %atomic-load68 = load atomic i8, i8* @cx seq_cst, align 1
  br label %atomic_cont69

atomic_cont69:                                    ; preds = %atomic_cont69, %atomic_exit66
  %103 = phi i8 [ %atomic-load68, %atomic_exit66 ], [ %106, %atomic_cont69 ]
  %conv71 = sext i8 %103 to i32
  %shr72 = ashr i32 %conv71, %conv67
  %conv73 = trunc i32 %shr72 to i8
  store i8 %conv73, i8* %atomic-temp70, align 1
  %104 = load i8, i8* %atomic-temp70, align 1
  %105 = cmpxchg i8* @cx, i8 %103, i8 %104 seq_cst seq_cst
  %106 = extractvalue { i8, i1 } %105, 0
  %107 = extractvalue { i8, i1 } %105, 1
  br i1 %107, label %atomic_exit74, label %atomic_cont69

atomic_exit74:                                    ; preds = %atomic_cont69
  call void @__kmpc_flush(%ident_t* @0)
  %108 = load i16, i16* @sv, align 2
  %conv75 = sext i16 %108 to i32
  %atomic-load76 = load atomic i64, i64* @ulx monotonic, align 8
  br label %atomic_cont77

atomic_cont77:                                    ; preds = %atomic_cont77, %atomic_exit74
  %109 = phi i64 [ %atomic-load76, %atomic_exit74 ], [ %112, %atomic_cont77 ]
  %sh_prom = trunc i64 %109 to i32
  %shl79 = shl i32 %conv75, %sh_prom
  %conv80 = sext i32 %shl79 to i64
  store i64 %conv80, i64* %atomic-temp78, align 8
  %110 = load i64, i64* %atomic-temp78, align 8
  %111 = cmpxchg i64* @ulx, i64 %109, i64 %110 monotonic monotonic
  %112 = extractvalue { i64, i1 } %111, 0
  %113 = extractvalue { i64, i1 } %111, 1
  br i1 %113, label %atomic_exit81, label %atomic_cont77

atomic_exit81:                                    ; preds = %atomic_cont77
  %114 = load i16, i16* @usv, align 2
  %conv82 = zext i16 %114 to i64
  %atomic-load83 = load atomic i64, i64* @lx monotonic, align 8
  br label %atomic_cont84

atomic_cont84:                                    ; preds = %atomic_cont84, %atomic_exit81
  %115 = phi i64 [ %atomic-load83, %atomic_exit81 ], [ %118, %atomic_cont84 ]
  %rem = srem i64 %115, %conv82
  store i64 %rem, i64* %atomic-temp85, align 8
  %116 = load i64, i64* %atomic-temp85, align 8
  %117 = cmpxchg i64* @lx, i64 %115, i64 %116 monotonic monotonic
  %118 = extractvalue { i64, i1 } %117, 0
  %119 = extractvalue { i64, i1 } %117, 1
  br i1 %119, label %atomic_exit86, label %atomic_cont84

atomic_exit86:                                    ; preds = %atomic_cont84
  %120 = load i32, i32* @iv, align 4
  %121 = atomicrmw or i32* @uix, i32 %120 seq_cst
  call void @__kmpc_flush(%ident_t* @0)
  %122 = load i32, i32* @uiv, align 4
  %123 = atomicrmw and i32* @ix, i32 %122 monotonic
  %124 = load i64, i64* @lv, align 8
  %125 = bitcast { i32, i32 }* %atomic-temp87 to i8*
  invoke void @__atomic_load(i64 8, i8* bitcast ({ i32, i32 }* @cix to i8*), i8* %125, i32 0)
          to label %invoke.cont88 unwind label %terminate.lpad

invoke.cont88:                                    ; preds = %atomic_exit86
  br label %atomic_cont89

atomic_cont89:                                    ; preds = %invoke.cont97, %invoke.cont88
  %atomic-temp87.realp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp87, i32 0, i32 0
  %atomic-temp87.real = load i32, i32* %atomic-temp87.realp, align 4
  %atomic-temp87.imagp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp87, i32 0, i32 1
  %atomic-temp87.imag = load i32, i32* %atomic-temp87.imagp, align 4
  %conv91 = sext i32 %atomic-temp87.real to i64
  %conv92 = sext i32 %atomic-temp87.imag to i64
  %add.r93 = add i64 %124, %conv91
  %add.i94 = add i64 0, %conv92
  %conv95 = trunc i64 %add.r93 to i32
  %conv96 = trunc i64 %add.i94 to i32
  %atomic-temp90.realp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp90, i32 0, i32 0
  %atomic-temp90.imagp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp90, i32 0, i32 1
  store i32 %conv95, i32* %atomic-temp90.realp, align 4
  store i32 %conv96, i32* %atomic-temp90.imagp, align 4
  %126 = bitcast { i32, i32 }* %atomic-temp87 to i8*
  %127 = bitcast { i32, i32 }* %atomic-temp90 to i8*
  %call98 = invoke zeroext i1 @__atomic_compare_exchange(i64 8, i8* bitcast ({ i32, i32 }* @cix to i8*), i8* %126, i8* %127, i32 0, i32 0)
          to label %invoke.cont97 unwind label %terminate.lpad

invoke.cont97:                                    ; preds = %atomic_cont89
  br i1 %call98, label %atomic_exit99, label %atomic_cont89

atomic_exit99:                                    ; preds = %invoke.cont97
  %128 = load i64, i64* @ulv, align 8
  %conv100 = uitofp i64 %128 to float
  %atomic-load101 = load atomic i32, i32* bitcast (float* @fx to i32*) monotonic, align 4
  br label %atomic_cont102

atomic_cont102:                                   ; preds = %atomic_cont102, %atomic_exit99
  %129 = phi i32 [ %atomic-load101, %atomic_exit99 ], [ %134, %atomic_cont102 ]
  %130 = bitcast float* %atomic-temp103 to i32*
  %131 = bitcast i32 %129 to float
  %mul104 = fmul float %131, %conv100
  store float %mul104, float* %atomic-temp103, align 4
  %132 = load i32, i32* %130, align 4
  %133 = cmpxchg i32* bitcast (float* @fx to i32*), i32 %129, i32 %132 monotonic monotonic
  %134 = extractvalue { i32, i1 } %133, 0
  %135 = extractvalue { i32, i1 } %133, 1
  br i1 %135, label %atomic_exit105, label %atomic_cont102

atomic_exit105:                                   ; preds = %atomic_cont102
  %136 = load i64, i64* @llv, align 8
  %conv106 = sitofp i64 %136 to double
  %atomic-load107 = load atomic i64, i64* bitcast (double* @dx to i64*) monotonic, align 8
  br label %atomic_cont108

atomic_cont108:                                   ; preds = %atomic_cont108, %atomic_exit105
  %137 = phi i64 [ %atomic-load107, %atomic_exit105 ], [ %142, %atomic_cont108 ]
  %138 = bitcast double* %atomic-temp109 to i64*
  %139 = bitcast i64 %137 to double
  %div110 = fdiv double %139, %conv106
  store double %div110, double* %atomic-temp109, align 8
  %140 = load i64, i64* %138, align 8
  %141 = cmpxchg i64* bitcast (double* @dx to i64*), i64 %137, i64 %140 monotonic monotonic
  %142 = extractvalue { i64, i1 } %141, 0
  %143 = extractvalue { i64, i1 } %141, 1
  br i1 %143, label %atomic_exit111, label %atomic_cont108

atomic_exit111:                                   ; preds = %atomic_cont108
  %144 = load i64, i64* @ullv, align 8
  %conv112 = uitofp i64 %144 to x86_fp80
  %atomic-load113 = load atomic i128, i128* bitcast (x86_fp80* @ldx to i128*) monotonic, align 16
  br label %atomic_cont114

atomic_cont114:                                   ; preds = %atomic_cont114, %atomic_exit111
  %145 = phi i128 [ %atomic-load113, %atomic_exit111 ], [ %151, %atomic_cont114 ]
  %146 = bitcast x86_fp80* %atomic-temp115 to i128*
  store i128 %145, i128* %146, align 16
  %147 = bitcast x86_fp80* %atomic-temp116 to i128*
  store i128 %145, i128* %147, align 16
  %148 = load x86_fp80, x86_fp80* %atomic-temp116, align 16
  %sub117 = fsub x86_fp80 %148, %conv112
  store x86_fp80 %sub117, x86_fp80* %atomic-temp115, align 16
  %149 = load i128, i128* %146, align 16
  %150 = cmpxchg i128* bitcast (x86_fp80* @ldx to i128*), i128 %145, i128 %149 monotonic monotonic
  %151 = extractvalue { i128, i1 } %150, 0
  %152 = extractvalue { i128, i1 } %150, 1
  br i1 %152, label %atomic_exit118, label %atomic_cont114

atomic_exit118:                                   ; preds = %atomic_cont114
  %153 = load float, float* @fv, align 4
  %154 = bitcast { i32, i32 }* %atomic-temp119 to i8*
  invoke void @__atomic_load(i64 8, i8* bitcast ({ i32, i32 }* @cix to i8*), i8* %154, i32 0)
          to label %invoke.cont120 unwind label %terminate.lpad

invoke.cont120:                                   ; preds = %atomic_exit118
  br label %atomic_cont121

atomic_cont121:                                   ; preds = %invoke.cont128, %invoke.cont120
  %atomic-temp119.realp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp119, i32 0, i32 0
  %atomic-temp119.real = load i32, i32* %atomic-temp119.realp, align 4
  %atomic-temp119.imagp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp119, i32 0, i32 1
  %atomic-temp119.imag = load i32, i32* %atomic-temp119.imagp, align 4
  %conv123 = sitofp i32 %atomic-temp119.real to float
  %conv124 = sitofp i32 %atomic-temp119.imag to float
  %call125 = call <2 x float> @__divsc3(float %153, float 0.000000e+00, float %conv123, float %conv124) #3
  %155 = bitcast { float, float }* %coerce to <2 x float>*
  store <2 x float> %call125, <2 x float>* %155, align 4
  %coerce.realp = getelementptr inbounds { float, float }, { float, float }* %coerce, i32 0, i32 0
  %coerce.real = load float, float* %coerce.realp, align 4
  %coerce.imagp = getelementptr inbounds { float, float }, { float, float }* %coerce, i32 0, i32 1
  %coerce.imag = load float, float* %coerce.imagp, align 4
  %conv126 = fptosi float %coerce.real to i32
  %conv127 = fptosi float %coerce.imag to i32
  %atomic-temp122.realp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp122, i32 0, i32 0
  %atomic-temp122.imagp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp122, i32 0, i32 1
  store i32 %conv126, i32* %atomic-temp122.realp, align 4
  store i32 %conv127, i32* %atomic-temp122.imagp, align 4
  %156 = bitcast { i32, i32 }* %atomic-temp119 to i8*
  %157 = bitcast { i32, i32 }* %atomic-temp122 to i8*
  %call129 = invoke zeroext i1 @__atomic_compare_exchange(i64 8, i8* bitcast ({ i32, i32 }* @cix to i8*), i8* %156, i8* %157, i32 0, i32 0)
          to label %invoke.cont128 unwind label %terminate.lpad

invoke.cont128:                                   ; preds = %atomic_cont121
  br i1 %call129, label %atomic_exit130, label %atomic_cont121

atomic_exit130:                                   ; preds = %invoke.cont128
  %158 = load double, double* @dv, align 8
  %atomic-load131 = load atomic i16, i16* @sx monotonic, align 2
  br label %atomic_cont132

atomic_cont132:                                   ; preds = %atomic_cont132, %atomic_exit130
  %159 = phi i16 [ %atomic-load131, %atomic_exit130 ], [ %162, %atomic_cont132 ]
  %conv134 = sext i16 %159 to i32
  %conv135 = sitofp i32 %conv134 to double
  %add136 = fadd double %conv135, %158
  %conv137 = fptosi double %add136 to i16
  store i16 %conv137, i16* %atomic-temp133, align 2
  %160 = load i16, i16* %atomic-temp133, align 2
  %161 = cmpxchg i16* @sx, i16 %159, i16 %160 monotonic monotonic
  %162 = extractvalue { i16, i1 } %161, 0
  %163 = extractvalue { i16, i1 } %161, 1
  br i1 %163, label %atomic_exit138, label %atomic_cont132

atomic_exit138:                                   ; preds = %atomic_cont132
  %164 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load139 = load atomic i8, i8* @bx monotonic, align 1
  br label %atomic_cont140

atomic_cont140:                                   ; preds = %atomic_cont140, %atomic_exit138
  %165 = phi i8 [ %atomic-load139, %atomic_exit138 ], [ %168, %atomic_cont140 ]
  %tobool142 = trunc i8 %165 to i1
  %conv143 = zext i1 %tobool142 to i32
  %conv144 = sitofp i32 %conv143 to x86_fp80
  %mul145 = fmul x86_fp80 %164, %conv144
  %tobool146 = fcmp une x86_fp80 %mul145, 0xK00000000000000000000
  %frombool147 = zext i1 %tobool146 to i8
  store i8 %frombool147, i8* %atomic-temp141, align 1
  %166 = load i8, i8* %atomic-temp141, align 1
  %167 = cmpxchg i8* @bx, i8 %165, i8 %166 monotonic monotonic
  %168 = extractvalue { i8, i1 } %167, 0
  %169 = extractvalue { i8, i1 } %167, 1
  br i1 %169, label %atomic_exit148, label %atomic_cont140

atomic_exit148:                                   ; preds = %atomic_cont140
  %civ.real149 = load i32, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 0), align 4
  %civ.imag150 = load i32, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 1), align 4
  %atomic-load151 = load atomic i8, i8* @bx monotonic, align 1
  br label %atomic_cont152

atomic_cont152:                                   ; preds = %atomic_cont152, %atomic_exit148
  %170 = phi i8 [ %atomic-load151, %atomic_exit148 ], [ %173, %atomic_cont152 ]
  %tobool154 = trunc i8 %170 to i1
  %conv155 = zext i1 %tobool154 to i32
  %sub.r156 = sub i32 %civ.real149, %conv155
  %sub.i157 = sub i32 %civ.imag150, 0
  %tobool158 = icmp ne i32 %sub.r156, 0
  %tobool159 = icmp ne i32 %sub.i157, 0
  %tobool160 = or i1 %tobool158, %tobool159
  %frombool161 = zext i1 %tobool160 to i8
  store i8 %frombool161, i8* %atomic-temp153, align 1
  %171 = load i8, i8* %atomic-temp153, align 1
  %172 = cmpxchg i8* @bx, i8 %170, i8 %171 monotonic monotonic
  %173 = extractvalue { i8, i1 } %172, 0
  %174 = extractvalue { i8, i1 } %172, 1
  br i1 %174, label %atomic_exit162, label %atomic_cont152

atomic_exit162:                                   ; preds = %atomic_cont152
  %cfv.real163 = load float, float* getelementptr inbounds ({ float, float }, { float, float }* @cfv, i32 0, i32 0), align 4
  %cfv.imag164 = load float, float* getelementptr inbounds ({ float, float }, { float, float }* @cfv, i32 0, i32 1), align 4
  %atomic-load165 = load atomic i16, i16* @usx monotonic, align 2
  br label %atomic_cont166

atomic_cont166:                                   ; preds = %atomic_cont166, %atomic_exit162
  %175 = phi i16 [ %atomic-load165, %atomic_exit162 ], [ %179, %atomic_cont166 ]
  %conv168 = zext i16 %175 to i32
  %conv169 = sitofp i32 %conv168 to float
  %call170 = call <2 x float> @__divsc3(float %conv169, float 0.000000e+00, float %cfv.real163, float %cfv.imag164) #3
  %176 = bitcast { float, float }* %coerce171 to <2 x float>*
  store <2 x float> %call170, <2 x float>* %176, align 4
  %coerce171.realp = getelementptr inbounds { float, float }, { float, float }* %coerce171, i32 0, i32 0
  %coerce171.real = load float, float* %coerce171.realp, align 4
  %coerce171.imagp = getelementptr inbounds { float, float }, { float, float }* %coerce171, i32 0, i32 1
  %coerce171.imag = load float, float* %coerce171.imagp, align 4
  %conv172 = fptoui float %coerce171.real to i16
  store i16 %conv172, i16* %atomic-temp167, align 2
  %177 = load i16, i16* %atomic-temp167, align 2
  %178 = cmpxchg i16* @usx, i16 %175, i16 %177 monotonic monotonic
  %179 = extractvalue { i16, i1 } %178, 0
  %180 = extractvalue { i16, i1 } %178, 1
  br i1 %180, label %atomic_exit173, label %atomic_cont166

atomic_exit173:                                   ; preds = %atomic_cont166
  %cdv.real174 = load double, double* getelementptr inbounds ({ double, double }, { double, double }* @cdv, i32 0, i32 0), align 8
  %cdv.imag175 = load double, double* getelementptr inbounds ({ double, double }, { double, double }* @cdv, i32 0, i32 1), align 8
  %atomic-load176 = load atomic i64, i64* @llx monotonic, align 8
  br label %atomic_cont177

atomic_cont177:                                   ; preds = %atomic_cont177, %atomic_exit173
  %181 = phi i64 [ %atomic-load176, %atomic_exit173 ], [ %184, %atomic_cont177 ]
  %conv179 = sitofp i64 %181 to double
  %add.r180 = fadd double %conv179, %cdv.real174
  %add.i181 = fadd double 0.000000e+00, %cdv.imag175
  %conv182 = fptosi double %add.r180 to i64
  store i64 %conv182, i64* %atomic-temp178, align 8
  %182 = load i64, i64* %atomic-temp178, align 8
  %183 = cmpxchg i64* @llx, i64 %181, i64 %182 monotonic monotonic
  %184 = extractvalue { i64, i1 } %183, 0
  %185 = extractvalue { i64, i1 } %183, 1
  br i1 %185, label %atomic_exit183, label %atomic_cont177

atomic_exit183:                                   ; preds = %atomic_cont177
  %186 = load i16, i16* @sv, align 2
  %187 = load i8, i8* @bv, align 1
  %tobool184 = trunc i8 %187 to i1
  %conv185 = zext i1 %tobool184 to i32
  %atomic-load186 = load atomic i128, i128* bitcast (<4 x i32>* @int4x to i128*) monotonic, align 16
  br label %atomic_cont187

atomic_cont187:                                   ; preds = %atomic_cont187, %atomic_exit183
  %188 = phi i128 [ %atomic-load186, %atomic_exit183 ], [ %195, %atomic_cont187 ]
  %189 = bitcast <4 x i32>* %atomic-temp188 to i128*
  store i128 %188, i128* %189, align 16
  %190 = bitcast i128 %188 to <4 x i32>
  store <4 x i32> %190, <4 x i32>* %atomic-temp189, align 16
  %191 = load <4 x i32>, <4 x i32>* %atomic-temp189, align 16
  %vecext = extractelement <4 x i32> %191, i16 %186
  %or = or i32 %vecext, %conv185
  %192 = load <4 x i32>, <4 x i32>* %atomic-temp188, align 16
  %vecins = insertelement <4 x i32> %192, i32 %or, i16 %186
  store <4 x i32> %vecins, <4 x i32>* %atomic-temp188, align 16
  %193 = load i128, i128* %189, align 16
  %194 = cmpxchg i128* bitcast (<4 x i32>* @int4x to i128*), i128 %188, i128 %193 monotonic monotonic
  %195 = extractvalue { i128, i1 } %194, 0
  %196 = extractvalue { i128, i1 } %194, 1
  br i1 %196, label %atomic_exit190, label %atomic_cont187

atomic_exit190:                                   ; preds = %atomic_cont187
  %197 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load191 = load atomic i32, i32* bitcast (i8* getelementptr (i8, i8* bitcast (%struct.BitFields* @bfx to i8*), i64 4) to i32*) monotonic, align 4
  br label %atomic_cont192

atomic_cont192:                                   ; preds = %atomic_cont192, %atomic_exit190
  %198 = phi i32 [ %atomic-load191, %atomic_exit190 ], [ %201, %atomic_cont192 ]
  store i32 %198, i32* %atomic-temp193, align 4
  store i32 %198, i32* %atomic-temp194, align 4
  %bf.load = load i32, i32* %atomic-temp194, align 4
  %bf.shl = shl i32 %bf.load, 1
  %bf.ashr = ashr i32 %bf.shl, 1
  %conv195 = sitofp i32 %bf.ashr to x86_fp80
  %sub196 = fsub x86_fp80 %conv195, %197
  %conv197 = fptosi x86_fp80 %sub196 to i32
  %bf.load198 = load i32, i32* %atomic-temp193, align 4
  %bf.value = and i32 %conv197, 2147483647
  %bf.clear = and i32 %bf.load198, -2147483648
  %bf.set = or i32 %bf.clear, %bf.value
  store i32 %bf.set, i32* %atomic-temp193, align 4
  %199 = load i32, i32* %atomic-temp193, align 4
  %200 = cmpxchg i32* bitcast (i8* getelementptr (i8, i8* bitcast (%struct.BitFields* @bfx to i8*), i64 4) to i32*), i32 %198, i32 %199 monotonic monotonic
  %201 = extractvalue { i32, i1 } %200, 0
  %202 = extractvalue { i32, i1 } %200, 1
  br i1 %202, label %atomic_exit199, label %atomic_cont192

atomic_exit199:                                   ; preds = %atomic_cont192
  %203 = load x86_fp80, x86_fp80* @ldv, align 16
  %204 = bitcast i32* %atomic-temp200 to i8*
  invoke void @__atomic_load(i64 4, i8* getelementptr (i8, i8* bitcast (%struct.BitFields_packed* @bfx_packed to i8*), i64 4), i8* %204, i32 0)
          to label %invoke.cont201 unwind label %terminate.lpad

invoke.cont201:                                   ; preds = %atomic_exit199
  br label %atomic_cont202

atomic_cont202:                                   ; preds = %invoke.cont215, %invoke.cont201
  %205 = load i32, i32* %atomic-temp200, align 1
  store i32 %205, i32* %atomic-temp203, align 1
  %206 = load i32, i32* %atomic-temp200, align 1
  store i32 %206, i32* %atomic-temp204, align 4
  %bf.load205 = load i32, i32* %atomic-temp204, align 1
  %bf.shl206 = shl i32 %bf.load205, 1
  %bf.ashr207 = ashr i32 %bf.shl206, 1
  %conv208 = sitofp i32 %bf.ashr207 to x86_fp80
  %mul209 = fmul x86_fp80 %conv208, %203
  %conv210 = fptosi x86_fp80 %mul209 to i32
  %bf.load211 = load i32, i32* %atomic-temp203, align 1
  %bf.value212 = and i32 %conv210, 2147483647
  %bf.clear213 = and i32 %bf.load211, -2147483648
  %bf.set214 = or i32 %bf.clear213, %bf.value212
  store i32 %bf.set214, i32* %atomic-temp203, align 1
  %207 = bitcast i32* %atomic-temp200 to i8*
  %208 = bitcast i32* %atomic-temp203 to i8*
  %call216 = invoke zeroext i1 @__atomic_compare_exchange(i64 4, i8* getelementptr (i8, i8* bitcast (%struct.BitFields_packed* @bfx_packed to i8*), i64 4), i8* %207, i8* %208, i32 0, i32 0)
          to label %invoke.cont215 unwind label %terminate.lpad

invoke.cont215:                                   ; preds = %atomic_cont202
  br i1 %call216, label %atomic_exit217, label %atomic_cont202

atomic_exit217:                                   ; preds = %invoke.cont215
  %209 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load218 = load atomic i32, i32* getelementptr inbounds (%struct.BitFields2, %struct.BitFields2* @bfx2, i32 0, i32 0) monotonic, align 4
  br label %atomic_cont219

atomic_cont219:                                   ; preds = %atomic_cont219, %atomic_exit217
  %210 = phi i32 [ %atomic-load218, %atomic_exit217 ], [ %213, %atomic_cont219 ]
  store i32 %210, i32* %atomic-temp220, align 4
  store i32 %210, i32* %atomic-temp221, align 4
  %bf.load222 = load i32, i32* %atomic-temp221, align 4
  %bf.ashr223 = ashr i32 %bf.load222, 31
  %conv224 = sitofp i32 %bf.ashr223 to x86_fp80
  %sub225 = fsub x86_fp80 %conv224, %209
  %conv226 = fptosi x86_fp80 %sub225 to i32
  %bf.load227 = load i32, i32* %atomic-temp220, align 4
  %bf.value228 = and i32 %conv226, 1
  %bf.shl229 = shl i32 %bf.value228, 31
  %bf.clear230 = and i32 %bf.load227, 2147483647
  %bf.set231 = or i32 %bf.clear230, %bf.shl229
  store i32 %bf.set231, i32* %atomic-temp220, align 4
  %211 = load i32, i32* %atomic-temp220, align 4
  %212 = cmpxchg i32* getelementptr inbounds (%struct.BitFields2, %struct.BitFields2* @bfx2, i32 0, i32 0), i32 %210, i32 %211 monotonic monotonic
  %213 = extractvalue { i32, i1 } %212, 0
  %214 = extractvalue { i32, i1 } %212, 1
  br i1 %214, label %atomic_exit232, label %atomic_cont219

atomic_exit232:                                   ; preds = %atomic_cont219
  %215 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load233 = load atomic i8, i8* getelementptr (i8, i8* bitcast (%struct.BitFields2_packed* @bfx2_packed to i8*), i64 3) monotonic, align 1
  br label %atomic_cont234

atomic_cont234:                                   ; preds = %atomic_cont234, %atomic_exit232
  %216 = phi i8 [ %atomic-load233, %atomic_exit232 ], [ %222, %atomic_cont234 ]
  %217 = bitcast i32* %atomic-temp235 to i8*
  store i8 %216, i8* %217, align 1
  %218 = bitcast i32* %atomic-temp236 to i8*
  store i8 %216, i8* %218, align 1
  %bf.load237 = load i8, i8* %218, align 1
  %bf.ashr238 = ashr i8 %bf.load237, 7
  %bf.cast = sext i8 %bf.ashr238 to i32
  %conv239 = sitofp i32 %bf.cast to x86_fp80
  %div240 = fdiv x86_fp80 %215, %conv239
  %conv241 = fptosi x86_fp80 %div240 to i32
  %219 = trunc i32 %conv241 to i8
  %bf.load242 = load i8, i8* %217, align 1
  %bf.value243 = and i8 %219, 1
  %bf.shl244 = shl i8 %bf.value243, 7
  %bf.clear245 = and i8 %bf.load242, 127
  %bf.set246 = or i8 %bf.clear245, %bf.shl244
  store i8 %bf.set246, i8* %217, align 1
  %220 = load i8, i8* %217, align 1
  %221 = cmpxchg i8* getelementptr (i8, i8* bitcast (%struct.BitFields2_packed* @bfx2_packed to i8*), i64 3), i8 %216, i8 %220 monotonic monotonic
  %222 = extractvalue { i8, i1 } %221, 0
  %223 = extractvalue { i8, i1 } %221, 1
  br i1 %223, label %atomic_exit247, label %atomic_cont234

atomic_exit247:                                   ; preds = %atomic_cont234
  %224 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load248 = load atomic i32, i32* getelementptr inbounds (%struct.BitFields3, %struct.BitFields3* @bfx3, i32 0, i32 0) monotonic, align 4
  br label %atomic_cont249

atomic_cont249:                                   ; preds = %atomic_cont249, %atomic_exit247
  %225 = phi i32 [ %atomic-load248, %atomic_exit247 ], [ %228, %atomic_cont249 ]
  store i32 %225, i32* %atomic-temp250, align 4
  store i32 %225, i32* %atomic-temp251, align 4
  %bf.load252 = load i32, i32* %atomic-temp251, align 4
  %bf.shl253 = shl i32 %bf.load252, 7
  %bf.ashr254 = ashr i32 %bf.shl253, 18
  %conv255 = sitofp i32 %bf.ashr254 to x86_fp80
  %div256 = fdiv x86_fp80 %conv255, %224
  %conv257 = fptosi x86_fp80 %div256 to i32
  %bf.load258 = load i32, i32* %atomic-temp250, align 4
  %bf.value259 = and i32 %conv257, 16383
  %bf.shl260 = shl i32 %bf.value259, 11
  %bf.clear261 = and i32 %bf.load258, -33552385
  %bf.set262 = or i32 %bf.clear261, %bf.shl260
  store i32 %bf.set262, i32* %atomic-temp250, align 4
  %226 = load i32, i32* %atomic-temp250, align 4
  %227 = cmpxchg i32* getelementptr inbounds (%struct.BitFields3, %struct.BitFields3* @bfx3, i32 0, i32 0), i32 %225, i32 %226 monotonic monotonic
  %228 = extractvalue { i32, i1 } %227, 0
  %229 = extractvalue { i32, i1 } %227, 1
  br i1 %229, label %atomic_exit263, label %atomic_cont249

atomic_exit263:                                   ; preds = %atomic_cont249
  %230 = load x86_fp80, x86_fp80* @ldv, align 16
  %231 = bitcast i32* %atomic-temp264 to i24*
  %232 = bitcast i24* %231 to i8*
  invoke void @__atomic_load(i64 3, i8* getelementptr (i8, i8* bitcast (%struct.BitFields3_packed* @bfx3_packed to i8*), i64 1), i8* %232, i32 0)
          to label %invoke.cont265 unwind label %terminate.lpad

invoke.cont265:                                   ; preds = %atomic_exit263
  br label %atomic_cont266

atomic_cont266:                                   ; preds = %invoke.cont281, %invoke.cont265
  %233 = bitcast i32* %atomic-temp267 to i24*
  %234 = load i24, i24* %231, align 1
  store i24 %234, i24* %233, align 1
  %235 = load i24, i24* %231, align 1
  %236 = bitcast i32* %atomic-temp268 to i24*
  store i24 %235, i24* %236, align 1
  %bf.load269 = load i24, i24* %236, align 1
  %bf.shl270 = shl i24 %bf.load269, 7
  %bf.ashr271 = ashr i24 %bf.shl270, 10
  %bf.cast272 = sext i24 %bf.ashr271 to i32
  %conv273 = sitofp i32 %bf.cast272 to x86_fp80
  %add274 = fadd x86_fp80 %conv273, %230
  %conv275 = fptosi x86_fp80 %add274 to i32
  %237 = trunc i32 %conv275 to i24
  %bf.load276 = load i24, i24* %233, align 1
  %bf.value277 = and i24 %237, 16383
  %bf.shl278 = shl i24 %bf.value277, 3
  %bf.clear279 = and i24 %bf.load276, -131065
  %bf.set280 = or i24 %bf.clear279, %bf.shl278
  store i24 %bf.set280, i24* %233, align 1
  %238 = bitcast i24* %231 to i8*
  %239 = bitcast i24* %233 to i8*
  %call282 = invoke zeroext i1 @__atomic_compare_exchange(i64 3, i8* getelementptr (i8, i8* bitcast (%struct.BitFields3_packed* @bfx3_packed to i8*), i64 1), i8* %238, i8* %239, i32 0, i32 0)
          to label %invoke.cont281 unwind label %terminate.lpad

invoke.cont281:                                   ; preds = %atomic_cont266
  br i1 %call282, label %atomic_exit283, label %atomic_cont266

atomic_exit283:                                   ; preds = %invoke.cont281
  %240 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load284 = load atomic i64, i64* bitcast (%struct.BitFields4* @bfx4 to i64*) monotonic, align 8
  br label %atomic_cont285

atomic_cont285:                                   ; preds = %atomic_cont285, %atomic_exit283
  %241 = phi i64 [ %atomic-load284, %atomic_exit283 ], [ %245, %atomic_cont285 ]
  store i64 %241, i64* %atomic-temp286, align 8
  store i64 %241, i64* %atomic-temp287, align 8
  %bf.load288 = load i64, i64* %atomic-temp287, align 8
  %bf.shl289 = shl i64 %bf.load288, 47
  %bf.ashr290 = ashr i64 %bf.shl289, 63
  %bf.cast291 = trunc i64 %bf.ashr290 to i32
  %conv292 = sitofp i32 %bf.cast291 to x86_fp80
  %mul293 = fmul x86_fp80 %conv292, %240
  %conv294 = fptosi x86_fp80 %mul293 to i32
  %242 = zext i32 %conv294 to i64
  %bf.load295 = load i64, i64* %atomic-temp286, align 8
  %bf.value296 = and i64 %242, 1
  %bf.shl297 = shl i64 %bf.value296, 16
  %bf.clear298 = and i64 %bf.load295, -65537
  %bf.set299 = or i64 %bf.clear298, %bf.shl297
  store i64 %bf.set299, i64* %atomic-temp286, align 8
  %243 = load i64, i64* %atomic-temp286, align 8
  %244 = cmpxchg i64* bitcast (%struct.BitFields4* @bfx4 to i64*), i64 %241, i64 %243 monotonic monotonic
  %245 = extractvalue { i64, i1 } %244, 0
  %246 = extractvalue { i64, i1 } %244, 1
  br i1 %246, label %atomic_exit300, label %atomic_cont285

atomic_exit300:                                   ; preds = %atomic_cont285
  %247 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load301 = load atomic i8, i8* getelementptr inbounds (%struct.BitFields4_packed, %struct.BitFields4_packed* @bfx4_packed, i32 0, i32 0, i64 2) monotonic, align 1
  br label %atomic_cont302

atomic_cont302:                                   ; preds = %atomic_cont302, %atomic_exit300
  %248 = phi i8 [ %atomic-load301, %atomic_exit300 ], [ %254, %atomic_cont302 ]
  %249 = bitcast i32* %atomic-temp303 to i8*
  store i8 %248, i8* %249, align 1
  %250 = bitcast i32* %atomic-temp304 to i8*
  store i8 %248, i8* %250, align 1
  %bf.load305 = load i8, i8* %250, align 1
  %bf.shl306 = shl i8 %bf.load305, 7
  %bf.ashr307 = ashr i8 %bf.shl306, 7
  %bf.cast308 = sext i8 %bf.ashr307 to i32
  %conv309 = sitofp i32 %bf.cast308 to x86_fp80
  %sub310 = fsub x86_fp80 %conv309, %247
  %conv311 = fptosi x86_fp80 %sub310 to i32
  %251 = trunc i32 %conv311 to i8
  %bf.load312 = load i8, i8* %249, align 1
  %bf.value313 = and i8 %251, 1
  %bf.clear314 = and i8 %bf.load312, -2
  %bf.set315 = or i8 %bf.clear314, %bf.value313
  store i8 %bf.set315, i8* %249, align 1
  %252 = load i8, i8* %249, align 1
  %253 = cmpxchg i8* getelementptr inbounds (%struct.BitFields4_packed, %struct.BitFields4_packed* @bfx4_packed, i32 0, i32 0, i64 2), i8 %248, i8 %252 monotonic monotonic
  %254 = extractvalue { i8, i1 } %253, 0
  %255 = extractvalue { i8, i1 } %253, 1
  br i1 %255, label %atomic_exit316, label %atomic_cont302

atomic_exit316:                                   ; preds = %atomic_cont302
  %256 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load317 = load atomic i64, i64* bitcast (%struct.BitFields4* @bfx4 to i64*) monotonic, align 8
  br label %atomic_cont318

atomic_cont318:                                   ; preds = %atomic_cont318, %atomic_exit316
  %257 = phi i64 [ %atomic-load317, %atomic_exit316 ], [ %260, %atomic_cont318 ]
  store i64 %257, i64* %atomic-temp319, align 8
  store i64 %257, i64* %atomic-temp320, align 8
  %bf.load321 = load i64, i64* %atomic-temp320, align 8
  %bf.shl322 = shl i64 %bf.load321, 40
  %bf.ashr323 = ashr i64 %bf.shl322, 57
  %conv324 = sitofp i64 %bf.ashr323 to x86_fp80
  %div325 = fdiv x86_fp80 %conv324, %256
  %conv326 = fptosi x86_fp80 %div325 to i64
  %bf.load327 = load i64, i64* %atomic-temp319, align 8
  %bf.value328 = and i64 %conv326, 127
  %bf.shl329 = shl i64 %bf.value328, 17
  %bf.clear330 = and i64 %bf.load327, -16646145
  %bf.set331 = or i64 %bf.clear330, %bf.shl329
  store i64 %bf.set331, i64* %atomic-temp319, align 8
  %258 = load i64, i64* %atomic-temp319, align 8
  %259 = cmpxchg i64* bitcast (%struct.BitFields4* @bfx4 to i64*), i64 %257, i64 %258 monotonic monotonic
  %260 = extractvalue { i64, i1 } %259, 0
  %261 = extractvalue { i64, i1 } %259, 1
  br i1 %261, label %atomic_exit332, label %atomic_cont318

atomic_exit332:                                   ; preds = %atomic_cont318
  %262 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load333 = load atomic i8, i8* getelementptr inbounds (%struct.BitFields4_packed, %struct.BitFields4_packed* @bfx4_packed, i32 0, i32 0, i64 2) monotonic, align 1
  br label %atomic_cont334

atomic_cont334:                                   ; preds = %atomic_cont334, %atomic_exit332
  %263 = phi i8 [ %atomic-load333, %atomic_exit332 ], [ %269, %atomic_cont334 ]
  %264 = bitcast i64* %atomic-temp335 to i8*
  store i8 %263, i8* %264, align 1
  %265 = bitcast i64* %atomic-temp336 to i8*
  store i8 %263, i8* %265, align 1
  %bf.load337 = load i8, i8* %265, align 1
  %bf.ashr338 = ashr i8 %bf.load337, 1
  %bf.cast339 = sext i8 %bf.ashr338 to i64
  %conv340 = sitofp i64 %bf.cast339 to x86_fp80
  %add341 = fadd x86_fp80 %conv340, %262
  %conv342 = fptosi x86_fp80 %add341 to i64
  %266 = trunc i64 %conv342 to i8
  %bf.load343 = load i8, i8* %264, align 1
  %bf.value344 = and i8 %266, 127
  %bf.shl345 = shl i8 %bf.value344, 1
  %bf.clear346 = and i8 %bf.load343, 1
  %bf.set347 = or i8 %bf.clear346, %bf.shl345
  store i8 %bf.set347, i8* %264, align 1
  %267 = load i8, i8* %264, align 1
  %268 = cmpxchg i8* getelementptr inbounds (%struct.BitFields4_packed, %struct.BitFields4_packed* @bfx4_packed, i32 0, i32 0, i64 2), i8 %263, i8 %267 monotonic monotonic
  %269 = extractvalue { i8, i1 } %268, 0
  %270 = extractvalue { i8, i1 } %268, 1
  br i1 %270, label %atomic_exit348, label %atomic_cont334

atomic_exit348:                                   ; preds = %atomic_cont334
  %271 = load i64, i64* @ulv, align 8
  %conv349 = uitofp i64 %271 to float
  %atomic-load350 = load atomic i64, i64* bitcast (<2 x float>* @float2x to i64*) monotonic, align 8
  br label %atomic_cont351

atomic_cont351:                                   ; preds = %atomic_cont351, %atomic_exit348
  %272 = phi i64 [ %atomic-load350, %atomic_exit348 ], [ %281, %atomic_cont351 ]
  %273 = bitcast <2 x float>* %atomic-temp352 to i64*
  store i64 %272, i64* %273, align 8
  %274 = bitcast i64 %272 to <2 x float>
  store <2 x float> %274, <2 x float>* %atomic-temp353, align 8
  %275 = load <2 x float>, <2 x float>* %atomic-temp353, align 8
  %276 = extractelement <2 x float> %275, i64 0
  %sub354 = fsub float %conv349, %276
  %277 = load <2 x float>, <2 x float>* %atomic-temp352, align 8
  %278 = insertelement <2 x float> %277, float %sub354, i64 0
  store <2 x float> %278, <2 x float>* %atomic-temp352, align 8
  %279 = load i64, i64* %273, align 8
  %280 = cmpxchg i64* bitcast (<2 x float>* @float2x to i64*), i64 %272, i64 %279 monotonic monotonic
  %281 = extractvalue { i64, i1 } %280, 0
  %282 = extractvalue { i64, i1 } %280, 1
  br i1 %282, label %atomic_exit355, label %atomic_cont351

atomic_exit355:                                   ; preds = %atomic_cont351
  %283 = load double, double* @dv, align 8
  %284 = call i32 @llvm.read_register.i32(metadata !0)
  %conv356 = sitofp i32 %284 to double
  %div357 = fdiv double %283, %conv356
  %conv358 = fptosi double %div357 to i32
  call void @llvm.write_register.i32(metadata !0, i32 %conv358)
  call void @__kmpc_flush(%ident_t* @0)
  ret i32 0

terminate.lpad:                                   ; preds = %atomic_cont266, %atomic_exit263, %atomic_cont202, %atomic_exit199, %atomic_cont121, %atomic_exit118, %atomic_cont89, %atomic_exit86, %atomic_cont53, %atomic_exit50, %atomic_cont46, %atomic_exit43, %atomic_cont40, %atomic_exit38
  %285 = landingpad { i8*, i32 }
          catch i8* null
  %286 = extractvalue { i8*, i32 } %285, 0
  call void @__clang_call_terminate(i8* %286) #4
  unreachable
}

declare void @__atomic_load(i64, i8*, i8*, i32)

declare i32 @__gxx_personality_v0(...)

; Function Attrs: noinline noreturn nounwind
define linkonce_odr hidden void @__clang_call_terminate(i8*) #1 comdat {
  %2 = call i8* @__cxa_begin_catch(i8* %0) #3
  call void @_ZSt9terminatev() #4
  unreachable
}

declare i8* @__cxa_begin_catch(i8*)

declare void @_ZSt9terminatev()

declare i1 @__atomic_compare_exchange(i64, i8*, i8*, i8*, i32, i32)

declare void @__kmpc_flush(%ident_t*)

declare <2 x float> @__divsc3(float, float, float, float)

; Function Attrs: nounwind readonly
declare i32 @llvm.read_register.i32(metadata) #2

; Function Attrs: nounwind
declare void @llvm.write_register.i32(metadata, i32) #3

attributes #0 = { norecurse uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline noreturn nounwind }
attributes #2 = { nounwind readonly }
attributes #3 = { nounwind }
attributes #4 = { noreturn nounwind }

!llvm.named.register.esp = !{!0}
!llvm.ident = !{!1}

!0 = !{!"esp"}
!1 = !{!"clang version 3.9.0 "}
