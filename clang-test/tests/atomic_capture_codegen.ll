; ModuleID = 'atomic_capture_codegen.cpp'
source_filename = "atomic_capture_codegen.cpp"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%ident_t = type { i32, i32, i32, i32, i8* }
%struct.BitFields = type { i64 }
%struct.BitFields_packed = type { i64 }
%struct.BitFields2 = type { i32 }
%struct.BitFields2_packed = type { i32 }
%struct.BitFields3 = type { i32 }
%struct.BitFields3_packed = type { i32 }
%struct.BitFields4 = type { i24, [4 x i8] }
%struct.BitFields4_packed = type { [3 x i8] }

$__clang_call_terminate = comdat any

@bv = global i8 0, align 1
@bx = global i8 0, align 1
@cv = global i8 0, align 1
@cx = global i8 0, align 1
@ucv = global i8 0, align 1
@ucx = global i8 0, align 1
@sv = global i16 0, align 2
@sx = global i16 0, align 2
@usx = global i16 0, align 2
@usv = global i16 0, align 2
@uiv = global i32 0, align 4
@ix = global i32 0, align 4
@iv = global i32 0, align 4
@uix = global i32 0, align 4
@ulv = global i64 0, align 8
@lx = global i64 0, align 8
@lv = global i64 0, align 8
@ulx = global i64 0, align 8
@ullv = global i64 0, align 8
@llx = global i64 0, align 8
@llv = global i64 0, align 8
@ullx = global i64 0, align 8
@dv = global double 0.000000e+00, align 8
@fx = global float 0.000000e+00, align 4
@fv = global float 0.000000e+00, align 4
@dx = global double 0.000000e+00, align 8
@ldx = global x86_fp80 0xK00000000000000000000, align 16
@ldv = global x86_fp80 0xK00000000000000000000, align 16
@cfv = global { float, float } zeroinitializer, align 4
@cix = global { i32, i32 } zeroinitializer, align 4
@civ = global { i32, i32 } zeroinitializer, align 4
@cfx = global { float, float } zeroinitializer, align 4
@cdx = global { double, double } zeroinitializer, align 8
@cdv = global { double, double } zeroinitializer, align 8
@.str = private unnamed_addr constant [23 x i8] c";unknown;unknown;0;0;;\00", align 1
@0 = private unnamed_addr constant %ident_t { i32 0, i32 2, i32 0, i32 0, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i32 0, i32 0) }, align 8
@int4x = global <4 x i32> zeroinitializer, align 16
@bfx = global %struct.BitFields zeroinitializer, align 4
@bfx_packed = global %struct.BitFields_packed zeroinitializer, align 1
@bfx2 = global %struct.BitFields2 zeroinitializer, align 4
@bfx2_packed = global %struct.BitFields2_packed zeroinitializer, align 1
@bfx3 = global %struct.BitFields3 zeroinitializer, align 4
@bfx3_packed = global %struct.BitFields3_packed zeroinitializer, align 1
@bfx4 = global %struct.BitFields4 zeroinitializer, align 8
@bfx4_packed = global %struct.BitFields4_packed zeroinitializer, align 1
@float2x = global <2 x float> zeroinitializer, align 8

; Function Attrs: norecurse uwtable
define i32 @main() #0 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %retval = alloca i32, align 4
  %atomic-temp = alloca i16, align 2
  %atomic-temp10 = alloca i32, align 4
  %atomic-temp14 = alloca i32, align 4
  %atomic-temp18 = alloca i32, align 4
  %atomic-temp22 = alloca i64, align 8
  %atomic-temp26 = alloca float, align 4
  %atomic-temp32 = alloca double, align 8
  %atomic-temp38 = alloca x86_fp80, align 16
  %atomic-temp39 = alloca x86_fp80, align 16
  %atomic-temp43 = alloca { i32, i32 }, align 4
  %atomic-temp45 = alloca { i32, i32 }, align 4
  %atomic-temp50 = alloca { float, float }, align 4
  %atomic-temp53 = alloca { float, float }, align 4
  %atomic-temp59 = alloca { double, double }, align 8
  %atomic-temp62 = alloca { double, double }, align 8
  %atomic-temp73 = alloca i8, align 1
  %atomic-temp83 = alloca i8, align 1
  %atomic-temp91 = alloca i64, align 8
  %atomic-temp98 = alloca i64, align 8
  %atomic-temp102 = alloca { i32, i32 }, align 4
  %atomic-temp105 = alloca { i32, i32 }, align 4
  %atomic-temp118 = alloca float, align 4
  %atomic-temp124 = alloca double, align 8
  %atomic-temp130 = alloca x86_fp80, align 16
  %atomic-temp131 = alloca x86_fp80, align 16
  %atomic-temp134 = alloca { i32, i32 }, align 4
  %atomic-temp137 = alloca { i32, i32 }, align 4
  %coerce = alloca { float, float }, align 4
  %atomic-temp148 = alloca i16, align 2
  %atomic-temp156 = alloca i8, align 1
  %atomic-temp169 = alloca i8, align 1
  %atomic-temp184 = alloca i16, align 2
  %coerce188 = alloca { float, float }, align 4
  %atomic-temp195 = alloca i64, align 8
  %atomic-temp205 = alloca <4 x i32>, align 16
  %atomic-temp206 = alloca <4 x i32>, align 16
  %atomic-temp211 = alloca i32, align 4
  %atomic-temp212 = alloca i32, align 4
  %atomic-temp218 = alloca i32, align 1
  %atomic-temp221 = alloca i32, align 1
  %atomic-temp222 = alloca i32, align 1
  %atomic-temp238 = alloca i32, align 4
  %atomic-temp239 = alloca i32, align 4
  %atomic-temp253 = alloca i32, align 1
  %atomic-temp254 = alloca i32, align 1
  %atomic-temp268 = alloca i32, align 4
  %atomic-temp269 = alloca i32, align 4
  %atomic-temp282 = alloca i32, align 1
  %atomic-temp285 = alloca i32, align 1
  %atomic-temp286 = alloca i32, align 1
  %atomic-temp304 = alloca i64, align 8
  %atomic-temp305 = alloca i64, align 8
  %atomic-temp321 = alloca i32, align 1
  %atomic-temp322 = alloca i32, align 1
  %atomic-temp337 = alloca i64, align 8
  %atomic-temp338 = alloca i64, align 8
  %atomic-temp354 = alloca i64, align 1
  %atomic-temp355 = alloca i64, align 1
  %atomic-temp372 = alloca <2 x float>, align 8
  %atomic-temp373 = alloca <2 x float>, align 8
  store i32 0, i32* %retval, align 4
  %0 = atomicrmw add i8* @bx, i8 1 monotonic
  store i8 %0, i8* @bv, align 1
  %1 = atomicrmw add i8* @cx, i8 1 monotonic
  %conv = sext i8 %1 to i32
  %add = add nsw i32 %conv, 1
  %conv1 = trunc i32 %add to i8
  store i8 %conv1, i8* @cv, align 1
  %2 = atomicrmw sub i8* @ucx, i8 1 monotonic
  store i8 %2, i8* @ucv, align 1
  %3 = atomicrmw sub i16* @sx, i16 1 monotonic
  %conv2 = sext i16 %3 to i32
  %sub = sub nsw i32 %conv2, 1
  %conv3 = trunc i32 %sub to i16
  store i16 %conv3, i16* @sv, align 2
  %4 = load i16, i16* @usv, align 2
  %conv4 = zext i16 %4 to i32
  %atomic-load = load atomic i16, i16* @usx monotonic, align 2
  br label %atomic_cont

atomic_cont:                                      ; preds = %atomic_cont, %entry
  %5 = phi i16 [ %atomic-load, %entry ], [ %8, %atomic_cont ]
  %conv5 = zext i16 %5 to i32
  %add6 = add nsw i32 %conv5, %conv4
  %conv7 = trunc i32 %add6 to i16
  store i16 %conv7, i16* %atomic-temp, align 2
  %6 = load i16, i16* %atomic-temp, align 2
  %7 = cmpxchg i16* @usx, i16 %5, i16 %6 monotonic monotonic
  %8 = extractvalue { i16, i1 } %7, 0
  %9 = extractvalue { i16, i1 } %7, 1
  br i1 %9, label %atomic_exit, label %atomic_cont

atomic_exit:                                      ; preds = %atomic_cont
  store i16 %conv7, i16* @sv, align 2
  %10 = load i32, i32* @iv, align 4
  %atomic-load8 = load atomic i32, i32* @ix monotonic, align 4
  br label %atomic_cont9

atomic_cont9:                                     ; preds = %atomic_cont9, %atomic_exit
  %11 = phi i32 [ %atomic-load8, %atomic_exit ], [ %14, %atomic_cont9 ]
  %mul = mul nsw i32 %11, %10
  store i32 %mul, i32* %atomic-temp10, align 4
  %12 = load i32, i32* %atomic-temp10, align 4
  %13 = cmpxchg i32* @ix, i32 %11, i32 %12 monotonic monotonic
  %14 = extractvalue { i32, i1 } %13, 0
  %15 = extractvalue { i32, i1 } %13, 1
  br i1 %15, label %atomic_exit11, label %atomic_cont9

atomic_exit11:                                    ; preds = %atomic_cont9
  store i32 %mul, i32* @uiv, align 4
  %16 = load i32, i32* @uiv, align 4
  %17 = atomicrmw sub i32* @uix, i32 %16 monotonic
  store i32 %17, i32* @iv, align 4
  %18 = load i32, i32* @iv, align 4
  %atomic-load12 = load atomic i32, i32* @ix monotonic, align 4
  br label %atomic_cont13

atomic_cont13:                                    ; preds = %atomic_cont13, %atomic_exit11
  %19 = phi i32 [ %atomic-load12, %atomic_exit11 ], [ %22, %atomic_cont13 ]
  %shl = shl i32 %19, %18
  store i32 %shl, i32* %atomic-temp14, align 4
  %20 = load i32, i32* %atomic-temp14, align 4
  %21 = cmpxchg i32* @ix, i32 %19, i32 %20 monotonic monotonic
  %22 = extractvalue { i32, i1 } %21, 0
  %23 = extractvalue { i32, i1 } %21, 1
  br i1 %23, label %atomic_exit15, label %atomic_cont13

atomic_exit15:                                    ; preds = %atomic_cont13
  store i32 %shl, i32* @uiv, align 4
  %24 = load i32, i32* @uiv, align 4
  %atomic-load16 = load atomic i32, i32* @uix monotonic, align 4
  br label %atomic_cont17

atomic_cont17:                                    ; preds = %atomic_cont17, %atomic_exit15
  %25 = phi i32 [ %atomic-load16, %atomic_exit15 ], [ %28, %atomic_cont17 ]
  %shr = lshr i32 %25, %24
  store i32 %shr, i32* %atomic-temp18, align 4
  %26 = load i32, i32* %atomic-temp18, align 4
  %27 = cmpxchg i32* @uix, i32 %25, i32 %26 monotonic monotonic
  %28 = extractvalue { i32, i1 } %27, 0
  %29 = extractvalue { i32, i1 } %27, 1
  br i1 %29, label %atomic_exit19, label %atomic_cont17

atomic_exit19:                                    ; preds = %atomic_cont17
  store i32 %shr, i32* @iv, align 4
  %30 = load i64, i64* @lv, align 8
  %atomic-load20 = load atomic i64, i64* @lx monotonic, align 8
  br label %atomic_cont21

atomic_cont21:                                    ; preds = %atomic_cont21, %atomic_exit19
  %31 = phi i64 [ %atomic-load20, %atomic_exit19 ], [ %34, %atomic_cont21 ]
  %div = sdiv i64 %31, %30
  store i64 %div, i64* %atomic-temp22, align 8
  %32 = load i64, i64* %atomic-temp22, align 8
  %33 = cmpxchg i64* @lx, i64 %31, i64 %32 monotonic monotonic
  %34 = extractvalue { i64, i1 } %33, 0
  %35 = extractvalue { i64, i1 } %33, 1
  br i1 %35, label %atomic_exit23, label %atomic_cont21

atomic_exit23:                                    ; preds = %atomic_cont21
  store i64 %31, i64* @ulv, align 8
  %36 = load i64, i64* @ulv, align 8
  %37 = atomicrmw and i64* @ulx, i64 %36 monotonic
  %and = and i64 %37, %36
  store i64 %and, i64* @lv, align 8
  %38 = load i64, i64* @llv, align 8
  %39 = atomicrmw xor i64* @llx, i64 %38 monotonic
  %xor = xor i64 %39, %38
  store i64 %xor, i64* @ullv, align 8
  %40 = load i64, i64* @ullv, align 8
  %41 = atomicrmw or i64* @ullx, i64 %40 monotonic
  %or = or i64 %41, %40
  store i64 %or, i64* @llv, align 8
  %42 = load float, float* @fv, align 4
  %atomic-load24 = load atomic i32, i32* bitcast (float* @fx to i32*) monotonic, align 4
  br label %atomic_cont25

atomic_cont25:                                    ; preds = %atomic_cont25, %atomic_exit23
  %43 = phi i32 [ %atomic-load24, %atomic_exit23 ], [ %48, %atomic_cont25 ]
  %44 = bitcast float* %atomic-temp26 to i32*
  %45 = bitcast i32 %43 to float
  %add27 = fadd float %45, %42
  store float %add27, float* %atomic-temp26, align 4
  %46 = load i32, i32* %44, align 4
  %47 = cmpxchg i32* bitcast (float* @fx to i32*), i32 %43, i32 %46 monotonic monotonic
  %48 = extractvalue { i32, i1 } %47, 0
  %49 = extractvalue { i32, i1 } %47, 1
  br i1 %49, label %atomic_exit28, label %atomic_cont25

atomic_exit28:                                    ; preds = %atomic_cont25
  %conv29 = fpext float %add27 to double
  store double %conv29, double* @dv, align 8
  %50 = load double, double* @dv, align 8
  %atomic-load30 = load atomic i64, i64* bitcast (double* @dx to i64*) monotonic, align 8
  br label %atomic_cont31

atomic_cont31:                                    ; preds = %atomic_cont31, %atomic_exit28
  %51 = phi i64 [ %atomic-load30, %atomic_exit28 ], [ %56, %atomic_cont31 ]
  %52 = bitcast double* %atomic-temp32 to i64*
  %53 = bitcast i64 %51 to double
  %sub33 = fsub double %50, %53
  store double %sub33, double* %atomic-temp32, align 8
  %54 = load i64, i64* %52, align 8
  %55 = cmpxchg i64* bitcast (double* @dx to i64*), i64 %51, i64 %54 monotonic monotonic
  %56 = extractvalue { i64, i1 } %55, 0
  %57 = extractvalue { i64, i1 } %55, 1
  br i1 %57, label %atomic_exit34, label %atomic_cont31

atomic_exit34:                                    ; preds = %atomic_cont31
  %conv35 = fptrunc double %53 to float
  store float %conv35, float* @fv, align 4
  %58 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load36 = load atomic i128, i128* bitcast (x86_fp80* @ldx to i128*) monotonic, align 16
  br label %atomic_cont37

atomic_cont37:                                    ; preds = %atomic_cont37, %atomic_exit34
  %59 = phi i128 [ %atomic-load36, %atomic_exit34 ], [ %65, %atomic_cont37 ]
  %60 = bitcast x86_fp80* %atomic-temp38 to i128*
  store i128 %59, i128* %60, align 16
  %61 = bitcast x86_fp80* %atomic-temp39 to i128*
  store i128 %59, i128* %61, align 16
  %62 = load x86_fp80, x86_fp80* %atomic-temp39, align 16
  %mul40 = fmul x86_fp80 %62, %58
  store x86_fp80 %mul40, x86_fp80* %atomic-temp38, align 16
  %63 = load i128, i128* %60, align 16
  %64 = cmpxchg i128* bitcast (x86_fp80* @ldx to i128*), i128 %59, i128 %63 monotonic monotonic
  %65 = extractvalue { i128, i1 } %64, 0
  %66 = extractvalue { i128, i1 } %64, 1
  br i1 %66, label %atomic_exit41, label %atomic_cont37

atomic_exit41:                                    ; preds = %atomic_cont37
  %conv42 = fptrunc x86_fp80 %mul40 to double
  store double %conv42, double* @dv, align 8
  %civ.real = load i32, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 0), align 4
  %civ.imag = load i32, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 1), align 4
  %67 = bitcast { i32, i32 }* %atomic-temp43 to i8*
  invoke void @__atomic_load(i64 8, i8* bitcast ({ i32, i32 }* @cix to i8*), i8* %67, i32 0)
          to label %invoke.cont unwind label %terminate.lpad

invoke.cont:                                      ; preds = %atomic_exit41
  br label %atomic_cont44

atomic_cont44:                                    ; preds = %invoke.cont46, %invoke.cont
  %atomic-temp43.realp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp43, i32 0, i32 0
  %atomic-temp43.real = load i32, i32* %atomic-temp43.realp, align 4
  %atomic-temp43.imagp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp43, i32 0, i32 1
  %atomic-temp43.imag = load i32, i32* %atomic-temp43.imagp, align 4
  %68 = mul i32 %civ.real, %atomic-temp43.real
  %69 = mul i32 %civ.imag, %atomic-temp43.imag
  %70 = add i32 %68, %69
  %71 = mul i32 %atomic-temp43.real, %atomic-temp43.real
  %72 = mul i32 %atomic-temp43.imag, %atomic-temp43.imag
  %73 = add i32 %71, %72
  %74 = mul i32 %civ.imag, %atomic-temp43.real
  %75 = mul i32 %civ.real, %atomic-temp43.imag
  %76 = sub i32 %74, %75
  %77 = sdiv i32 %70, %73
  %78 = sdiv i32 %76, %73
  %atomic-temp45.realp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp45, i32 0, i32 0
  %atomic-temp45.imagp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp45, i32 0, i32 1
  store i32 %77, i32* %atomic-temp45.realp, align 4
  store i32 %78, i32* %atomic-temp45.imagp, align 4
  %79 = bitcast { i32, i32 }* %atomic-temp43 to i8*
  %80 = bitcast { i32, i32 }* %atomic-temp45 to i8*
  %call = invoke zeroext i1 @__atomic_compare_exchange(i64 8, i8* bitcast ({ i32, i32 }* @cix to i8*), i8* %79, i8* %80, i32 0, i32 0)
          to label %invoke.cont46 unwind label %terminate.lpad

invoke.cont46:                                    ; preds = %atomic_cont44
  br i1 %call, label %atomic_exit47, label %atomic_cont44

atomic_exit47:                                    ; preds = %invoke.cont46
  %conv48 = sitofp i32 %77 to float
  %conv49 = sitofp i32 %78 to float
  store float %conv48, float* getelementptr inbounds ({ float, float }, { float, float }* @cfv, i32 0, i32 0), align 4
  store float %conv49, float* getelementptr inbounds ({ float, float }, { float, float }* @cfv, i32 0, i32 1), align 4
  %cfv.real = load float, float* getelementptr inbounds ({ float, float }, { float, float }* @cfv, i32 0, i32 0), align 4
  %cfv.imag = load float, float* getelementptr inbounds ({ float, float }, { float, float }* @cfv, i32 0, i32 1), align 4
  %81 = bitcast { float, float }* %atomic-temp50 to i8*
  invoke void @__atomic_load(i64 8, i8* bitcast ({ float, float }* @cfx to i8*), i8* %81, i32 0)
          to label %invoke.cont51 unwind label %terminate.lpad

invoke.cont51:                                    ; preds = %atomic_exit47
  br label %atomic_cont52

atomic_cont52:                                    ; preds = %invoke.cont54, %invoke.cont51
  %atomic-temp50.realp = getelementptr inbounds { float, float }, { float, float }* %atomic-temp50, i32 0, i32 0
  %atomic-temp50.real = load float, float* %atomic-temp50.realp, align 4
  %atomic-temp50.imagp = getelementptr inbounds { float, float }, { float, float }* %atomic-temp50, i32 0, i32 1
  %atomic-temp50.imag = load float, float* %atomic-temp50.imagp, align 4
  %add.r = fadd float %cfv.real, %atomic-temp50.real
  %add.i = fadd float %cfv.imag, %atomic-temp50.imag
  %atomic-temp53.realp = getelementptr inbounds { float, float }, { float, float }* %atomic-temp53, i32 0, i32 0
  %atomic-temp53.imagp = getelementptr inbounds { float, float }, { float, float }* %atomic-temp53, i32 0, i32 1
  store float %add.r, float* %atomic-temp53.realp, align 4
  store float %add.i, float* %atomic-temp53.imagp, align 4
  %82 = bitcast { float, float }* %atomic-temp50 to i8*
  %83 = bitcast { float, float }* %atomic-temp53 to i8*
  %call55 = invoke zeroext i1 @__atomic_compare_exchange(i64 8, i8* bitcast ({ float, float }* @cfx to i8*), i8* %82, i8* %83, i32 0, i32 0)
          to label %invoke.cont54 unwind label %terminate.lpad

invoke.cont54:                                    ; preds = %atomic_cont52
  br i1 %call55, label %atomic_exit56, label %atomic_cont52

atomic_exit56:                                    ; preds = %invoke.cont54
  %conv57 = fptosi float %atomic-temp50.real to i32
  %conv58 = fptosi float %atomic-temp50.imag to i32
  store i32 %conv57, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 0), align 4
  store i32 %conv58, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 1), align 4
  %cdv.real = load double, double* getelementptr inbounds ({ double, double }, { double, double }* @cdv, i32 0, i32 0), align 8
  %cdv.imag = load double, double* getelementptr inbounds ({ double, double }, { double, double }* @cdv, i32 0, i32 1), align 8
  %84 = bitcast { double, double }* %atomic-temp59 to i8*
  invoke void @__atomic_load(i64 16, i8* bitcast ({ double, double }* @cdx to i8*), i8* %84, i32 5)
          to label %invoke.cont60 unwind label %terminate.lpad

invoke.cont60:                                    ; preds = %atomic_exit56
  br label %atomic_cont61

atomic_cont61:                                    ; preds = %invoke.cont63, %invoke.cont60
  %atomic-temp59.realp = getelementptr inbounds { double, double }, { double, double }* %atomic-temp59, i32 0, i32 0
  %atomic-temp59.real = load double, double* %atomic-temp59.realp, align 8
  %atomic-temp59.imagp = getelementptr inbounds { double, double }, { double, double }* %atomic-temp59, i32 0, i32 1
  %atomic-temp59.imag = load double, double* %atomic-temp59.imagp, align 8
  %sub.r = fsub double %atomic-temp59.real, %cdv.real
  %sub.i = fsub double %atomic-temp59.imag, %cdv.imag
  %atomic-temp62.realp = getelementptr inbounds { double, double }, { double, double }* %atomic-temp62, i32 0, i32 0
  %atomic-temp62.imagp = getelementptr inbounds { double, double }, { double, double }* %atomic-temp62, i32 0, i32 1
  store double %sub.r, double* %atomic-temp62.realp, align 8
  store double %sub.i, double* %atomic-temp62.imagp, align 8
  %85 = bitcast { double, double }* %atomic-temp59 to i8*
  %86 = bitcast { double, double }* %atomic-temp62 to i8*
  %call64 = invoke zeroext i1 @__atomic_compare_exchange(i64 16, i8* bitcast ({ double, double }* @cdx to i8*), i8* %85, i8* %86, i32 5, i32 5)
          to label %invoke.cont63 unwind label %terminate.lpad

invoke.cont63:                                    ; preds = %atomic_cont61
  br i1 %call64, label %atomic_exit65, label %atomic_cont61

atomic_exit65:                                    ; preds = %invoke.cont63
  %conv66 = fptrunc double %sub.r to float
  %conv67 = fptrunc double %sub.i to float
  store float %conv66, float* getelementptr inbounds ({ float, float }, { float, float }* @cfv, i32 0, i32 0), align 4
  store float %conv67, float* getelementptr inbounds ({ float, float }, { float, float }* @cfv, i32 0, i32 1), align 4
  call void @__kmpc_flush(%ident_t* @0)
  %87 = load i8, i8* @bv, align 1
  %tobool = trunc i8 %87 to i1
  %conv68 = zext i1 %tobool to i64
  %88 = atomicrmw and i64* @ulx, i64 %conv68 monotonic
  %and69 = and i64 %88, %conv68
  store i64 %and69, i64* @ulv, align 8
  %89 = load i8, i8* @cv, align 1
  %conv70 = sext i8 %89 to i32
  %atomic-load71 = load atomic i8, i8* @bx monotonic, align 1
  br label %atomic_cont72

atomic_cont72:                                    ; preds = %atomic_cont72, %atomic_exit65
  %90 = phi i8 [ %atomic-load71, %atomic_exit65 ], [ %93, %atomic_cont72 ]
  %tobool74 = trunc i8 %90 to i1
  %conv75 = zext i1 %tobool74 to i32
  %and76 = and i32 %conv70, %conv75
  %tobool77 = icmp ne i32 %and76, 0
  %frombool = zext i1 %tobool77 to i8
  store i8 %frombool, i8* %atomic-temp73, align 1
  %91 = load i8, i8* %atomic-temp73, align 1
  %92 = cmpxchg i8* @bx, i8 %90, i8 %91 monotonic monotonic
  %93 = extractvalue { i8, i1 } %92, 0
  %94 = extractvalue { i8, i1 } %92, 1
  br i1 %94, label %atomic_exit78, label %atomic_cont72

atomic_exit78:                                    ; preds = %atomic_cont72
  %frombool79 = zext i1 %tobool74 to i8
  store i8 %frombool79, i8* @bv, align 1
  %95 = load i8, i8* @ucv, align 1
  %conv80 = zext i8 %95 to i32
  %atomic-load81 = load atomic i8, i8* @cx seq_cst, align 1
  br label %atomic_cont82

atomic_cont82:                                    ; preds = %atomic_cont82, %atomic_exit78
  %96 = phi i8 [ %atomic-load81, %atomic_exit78 ], [ %99, %atomic_cont82 ]
  %conv84 = sext i8 %96 to i32
  %shr85 = ashr i32 %conv84, %conv80
  %conv86 = trunc i32 %shr85 to i8
  store i8 %conv86, i8* %atomic-temp83, align 1
  %97 = load i8, i8* %atomic-temp83, align 1
  %98 = cmpxchg i8* @cx, i8 %96, i8 %97 seq_cst seq_cst
  %99 = extractvalue { i8, i1 } %98, 0
  %100 = extractvalue { i8, i1 } %98, 1
  br i1 %100, label %atomic_exit87, label %atomic_cont82

atomic_exit87:                                    ; preds = %atomic_cont82
  store i8 %conv86, i8* @cv, align 1
  call void @__kmpc_flush(%ident_t* @0)
  %101 = load i16, i16* @sv, align 2
  %conv88 = sext i16 %101 to i32
  %atomic-load89 = load atomic i64, i64* @ulx monotonic, align 8
  br label %atomic_cont90

atomic_cont90:                                    ; preds = %atomic_cont90, %atomic_exit87
  %102 = phi i64 [ %atomic-load89, %atomic_exit87 ], [ %105, %atomic_cont90 ]
  %sh_prom = trunc i64 %102 to i32
  %shl92 = shl i32 %conv88, %sh_prom
  %conv93 = sext i32 %shl92 to i64
  store i64 %conv93, i64* %atomic-temp91, align 8
  %103 = load i64, i64* %atomic-temp91, align 8
  %104 = cmpxchg i64* @ulx, i64 %102, i64 %103 monotonic monotonic
  %105 = extractvalue { i64, i1 } %104, 0
  %106 = extractvalue { i64, i1 } %104, 1
  br i1 %106, label %atomic_exit94, label %atomic_cont90

atomic_exit94:                                    ; preds = %atomic_cont90
  store i64 %conv93, i64* @ulv, align 8
  %107 = load i16, i16* @usv, align 2
  %conv95 = zext i16 %107 to i64
  %atomic-load96 = load atomic i64, i64* @lx monotonic, align 8
  br label %atomic_cont97

atomic_cont97:                                    ; preds = %atomic_cont97, %atomic_exit94
  %108 = phi i64 [ %atomic-load96, %atomic_exit94 ], [ %111, %atomic_cont97 ]
  %rem = srem i64 %108, %conv95
  store i64 %rem, i64* %atomic-temp98, align 8
  %109 = load i64, i64* %atomic-temp98, align 8
  %110 = cmpxchg i64* @lx, i64 %108, i64 %109 monotonic monotonic
  %111 = extractvalue { i64, i1 } %110, 0
  %112 = extractvalue { i64, i1 } %110, 1
  br i1 %112, label %atomic_exit99, label %atomic_cont97

atomic_exit99:                                    ; preds = %atomic_cont97
  store i64 %108, i64* @lv, align 8
  %113 = load i32, i32* @iv, align 4
  %114 = atomicrmw or i32* @uix, i32 %113 seq_cst
  %or100 = or i32 %113, %114
  store i32 %or100, i32* @uiv, align 4
  call void @__kmpc_flush(%ident_t* @0)
  %115 = load i32, i32* @uiv, align 4
  %116 = atomicrmw and i32* @ix, i32 %115 monotonic
  %and101 = and i32 %116, %115
  store i32 %and101, i32* @iv, align 4
  %117 = load i64, i64* @lv, align 8
  %118 = bitcast { i32, i32 }* %atomic-temp102 to i8*
  invoke void @__atomic_load(i64 8, i8* bitcast ({ i32, i32 }* @cix to i8*), i8* %118, i32 0)
          to label %invoke.cont103 unwind label %terminate.lpad

invoke.cont103:                                   ; preds = %atomic_exit99
  br label %atomic_cont104

atomic_cont104:                                   ; preds = %invoke.cont112, %invoke.cont103
  %atomic-temp102.realp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp102, i32 0, i32 0
  %atomic-temp102.real = load i32, i32* %atomic-temp102.realp, align 4
  %atomic-temp102.imagp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp102, i32 0, i32 1
  %atomic-temp102.imag = load i32, i32* %atomic-temp102.imagp, align 4
  %conv106 = sext i32 %atomic-temp102.real to i64
  %conv107 = sext i32 %atomic-temp102.imag to i64
  %add.r108 = add i64 %117, %conv106
  %add.i109 = add i64 0, %conv107
  %conv110 = trunc i64 %add.r108 to i32
  %conv111 = trunc i64 %add.i109 to i32
  %atomic-temp105.realp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp105, i32 0, i32 0
  %atomic-temp105.imagp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp105, i32 0, i32 1
  store i32 %conv110, i32* %atomic-temp105.realp, align 4
  store i32 %conv111, i32* %atomic-temp105.imagp, align 4
  %119 = bitcast { i32, i32 }* %atomic-temp102 to i8*
  %120 = bitcast { i32, i32 }* %atomic-temp105 to i8*
  %call113 = invoke zeroext i1 @__atomic_compare_exchange(i64 8, i8* bitcast ({ i32, i32 }* @cix to i8*), i8* %119, i8* %120, i32 0, i32 0)
          to label %invoke.cont112 unwind label %terminate.lpad

invoke.cont112:                                   ; preds = %atomic_cont104
  br i1 %call113, label %atomic_exit114, label %atomic_cont104

atomic_exit114:                                   ; preds = %invoke.cont112
  store i32 %atomic-temp102.real, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 0), align 4
  store i32 %atomic-temp102.imag, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 1), align 4
  %121 = load i64, i64* @ulv, align 8
  %conv115 = uitofp i64 %121 to float
  %atomic-load116 = load atomic i32, i32* bitcast (float* @fx to i32*) monotonic, align 4
  br label %atomic_cont117

atomic_cont117:                                   ; preds = %atomic_cont117, %atomic_exit114
  %122 = phi i32 [ %atomic-load116, %atomic_exit114 ], [ %127, %atomic_cont117 ]
  %123 = bitcast float* %atomic-temp118 to i32*
  %124 = bitcast i32 %122 to float
  %mul119 = fmul float %124, %conv115
  store float %mul119, float* %atomic-temp118, align 4
  %125 = load i32, i32* %123, align 4
  %126 = cmpxchg i32* bitcast (float* @fx to i32*), i32 %122, i32 %125 monotonic monotonic
  %127 = extractvalue { i32, i1 } %126, 0
  %128 = extractvalue { i32, i1 } %126, 1
  br i1 %128, label %atomic_exit120, label %atomic_cont117

atomic_exit120:                                   ; preds = %atomic_cont117
  store float %mul119, float* @fv, align 4
  %129 = load i64, i64* @llv, align 8
  %conv121 = sitofp i64 %129 to double
  %atomic-load122 = load atomic i64, i64* bitcast (double* @dx to i64*) monotonic, align 8
  br label %atomic_cont123

atomic_cont123:                                   ; preds = %atomic_cont123, %atomic_exit120
  %130 = phi i64 [ %atomic-load122, %atomic_exit120 ], [ %135, %atomic_cont123 ]
  %131 = bitcast double* %atomic-temp124 to i64*
  %132 = bitcast i64 %130 to double
  %div125 = fdiv double %132, %conv121
  store double %div125, double* %atomic-temp124, align 8
  %133 = load i64, i64* %131, align 8
  %134 = cmpxchg i64* bitcast (double* @dx to i64*), i64 %130, i64 %133 monotonic monotonic
  %135 = extractvalue { i64, i1 } %134, 0
  %136 = extractvalue { i64, i1 } %134, 1
  br i1 %136, label %atomic_exit126, label %atomic_cont123

atomic_exit126:                                   ; preds = %atomic_cont123
  store double %div125, double* @dv, align 8
  %137 = load i64, i64* @ullv, align 8
  %conv127 = uitofp i64 %137 to x86_fp80
  %atomic-load128 = load atomic i128, i128* bitcast (x86_fp80* @ldx to i128*) monotonic, align 16
  br label %atomic_cont129

atomic_cont129:                                   ; preds = %atomic_cont129, %atomic_exit126
  %138 = phi i128 [ %atomic-load128, %atomic_exit126 ], [ %144, %atomic_cont129 ]
  %139 = bitcast x86_fp80* %atomic-temp130 to i128*
  store i128 %138, i128* %139, align 16
  %140 = bitcast x86_fp80* %atomic-temp131 to i128*
  store i128 %138, i128* %140, align 16
  %141 = load x86_fp80, x86_fp80* %atomic-temp131, align 16
  %sub132 = fsub x86_fp80 %141, %conv127
  store x86_fp80 %sub132, x86_fp80* %atomic-temp130, align 16
  %142 = load i128, i128* %139, align 16
  %143 = cmpxchg i128* bitcast (x86_fp80* @ldx to i128*), i128 %138, i128 %142 monotonic monotonic
  %144 = extractvalue { i128, i1 } %143, 0
  %145 = extractvalue { i128, i1 } %143, 1
  br i1 %145, label %atomic_exit133, label %atomic_cont129

atomic_exit133:                                   ; preds = %atomic_cont129
  store x86_fp80 %141, x86_fp80* @ldv, align 16
  %146 = load float, float* @fv, align 4
  %147 = bitcast { i32, i32 }* %atomic-temp134 to i8*
  invoke void @__atomic_load(i64 8, i8* bitcast ({ i32, i32 }* @cix to i8*), i8* %147, i32 0)
          to label %invoke.cont135 unwind label %terminate.lpad

invoke.cont135:                                   ; preds = %atomic_exit133
  br label %atomic_cont136

atomic_cont136:                                   ; preds = %invoke.cont143, %invoke.cont135
  %atomic-temp134.realp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp134, i32 0, i32 0
  %atomic-temp134.real = load i32, i32* %atomic-temp134.realp, align 4
  %atomic-temp134.imagp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp134, i32 0, i32 1
  %atomic-temp134.imag = load i32, i32* %atomic-temp134.imagp, align 4
  %conv138 = sitofp i32 %atomic-temp134.real to float
  %conv139 = sitofp i32 %atomic-temp134.imag to float
  %call140 = call <2 x float> @__divsc3(float %146, float 0.000000e+00, float %conv138, float %conv139) #3
  %148 = bitcast { float, float }* %coerce to <2 x float>*
  store <2 x float> %call140, <2 x float>* %148, align 4
  %coerce.realp = getelementptr inbounds { float, float }, { float, float }* %coerce, i32 0, i32 0
  %coerce.real = load float, float* %coerce.realp, align 4
  %coerce.imagp = getelementptr inbounds { float, float }, { float, float }* %coerce, i32 0, i32 1
  %coerce.imag = load float, float* %coerce.imagp, align 4
  %conv141 = fptosi float %coerce.real to i32
  %conv142 = fptosi float %coerce.imag to i32
  %atomic-temp137.realp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp137, i32 0, i32 0
  %atomic-temp137.imagp = getelementptr inbounds { i32, i32 }, { i32, i32 }* %atomic-temp137, i32 0, i32 1
  store i32 %conv141, i32* %atomic-temp137.realp, align 4
  store i32 %conv142, i32* %atomic-temp137.imagp, align 4
  %149 = bitcast { i32, i32 }* %atomic-temp134 to i8*
  %150 = bitcast { i32, i32 }* %atomic-temp137 to i8*
  %call144 = invoke zeroext i1 @__atomic_compare_exchange(i64 8, i8* bitcast ({ i32, i32 }* @cix to i8*), i8* %149, i8* %150, i32 0, i32 0)
          to label %invoke.cont143 unwind label %terminate.lpad

invoke.cont143:                                   ; preds = %atomic_cont136
  br i1 %call144, label %atomic_exit145, label %atomic_cont136

atomic_exit145:                                   ; preds = %invoke.cont143
  store i32 %conv141, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 0), align 4
  store i32 %conv142, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 1), align 4
  %151 = load double, double* @dv, align 8
  %atomic-load146 = load atomic i16, i16* @sx monotonic, align 2
  br label %atomic_cont147

atomic_cont147:                                   ; preds = %atomic_cont147, %atomic_exit145
  %152 = phi i16 [ %atomic-load146, %atomic_exit145 ], [ %155, %atomic_cont147 ]
  %conv149 = sext i16 %152 to i32
  %conv150 = sitofp i32 %conv149 to double
  %add151 = fadd double %conv150, %151
  %conv152 = fptosi double %add151 to i16
  store i16 %conv152, i16* %atomic-temp148, align 2
  %153 = load i16, i16* %atomic-temp148, align 2
  %154 = cmpxchg i16* @sx, i16 %152, i16 %153 monotonic monotonic
  %155 = extractvalue { i16, i1 } %154, 0
  %156 = extractvalue { i16, i1 } %154, 1
  br i1 %156, label %atomic_exit153, label %atomic_cont147

atomic_exit153:                                   ; preds = %atomic_cont147
  store i16 %conv152, i16* @sv, align 2
  %157 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load154 = load atomic i8, i8* @bx monotonic, align 1
  br label %atomic_cont155

atomic_cont155:                                   ; preds = %atomic_cont155, %atomic_exit153
  %158 = phi i8 [ %atomic-load154, %atomic_exit153 ], [ %161, %atomic_cont155 ]
  %tobool157 = trunc i8 %158 to i1
  %conv158 = zext i1 %tobool157 to i32
  %conv159 = sitofp i32 %conv158 to x86_fp80
  %mul160 = fmul x86_fp80 %157, %conv159
  %tobool161 = fcmp une x86_fp80 %mul160, 0xK00000000000000000000
  %frombool162 = zext i1 %tobool161 to i8
  store i8 %frombool162, i8* %atomic-temp156, align 1
  %159 = load i8, i8* %atomic-temp156, align 1
  %160 = cmpxchg i8* @bx, i8 %158, i8 %159 monotonic monotonic
  %161 = extractvalue { i8, i1 } %160, 0
  %162 = extractvalue { i8, i1 } %160, 1
  br i1 %162, label %atomic_exit163, label %atomic_cont155

atomic_exit163:                                   ; preds = %atomic_cont155
  %frombool164 = zext i1 %tobool157 to i8
  store i8 %frombool164, i8* @bv, align 1
  %civ.real165 = load i32, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 0), align 4
  %civ.imag166 = load i32, i32* getelementptr inbounds ({ i32, i32 }, { i32, i32 }* @civ, i32 0, i32 1), align 4
  %atomic-load167 = load atomic i8, i8* @bx monotonic, align 1
  br label %atomic_cont168

atomic_cont168:                                   ; preds = %atomic_cont168, %atomic_exit163
  %163 = phi i8 [ %atomic-load167, %atomic_exit163 ], [ %166, %atomic_cont168 ]
  %tobool170 = trunc i8 %163 to i1
  %conv171 = zext i1 %tobool170 to i32
  %sub.r172 = sub i32 %civ.real165, %conv171
  %sub.i173 = sub i32 %civ.imag166, 0
  %tobool174 = icmp ne i32 %sub.r172, 0
  %tobool175 = icmp ne i32 %sub.i173, 0
  %tobool176 = or i1 %tobool174, %tobool175
  %frombool177 = zext i1 %tobool176 to i8
  store i8 %frombool177, i8* %atomic-temp169, align 1
  %164 = load i8, i8* %atomic-temp169, align 1
  %165 = cmpxchg i8* @bx, i8 %163, i8 %164 monotonic monotonic
  %166 = extractvalue { i8, i1 } %165, 0
  %167 = extractvalue { i8, i1 } %165, 1
  br i1 %167, label %atomic_exit178, label %atomic_cont168

atomic_exit178:                                   ; preds = %atomic_cont168
  %frombool179 = zext i1 %tobool176 to i8
  store i8 %frombool179, i8* @bv, align 1
  %cfv.real180 = load float, float* getelementptr inbounds ({ float, float }, { float, float }* @cfv, i32 0, i32 0), align 4
  %cfv.imag181 = load float, float* getelementptr inbounds ({ float, float }, { float, float }* @cfv, i32 0, i32 1), align 4
  %atomic-load182 = load atomic i16, i16* @usx monotonic, align 2
  br label %atomic_cont183

atomic_cont183:                                   ; preds = %atomic_cont183, %atomic_exit178
  %168 = phi i16 [ %atomic-load182, %atomic_exit178 ], [ %172, %atomic_cont183 ]
  %conv185 = zext i16 %168 to i32
  %conv186 = sitofp i32 %conv185 to float
  %call187 = call <2 x float> @__divsc3(float %conv186, float 0.000000e+00, float %cfv.real180, float %cfv.imag181) #3
  %169 = bitcast { float, float }* %coerce188 to <2 x float>*
  store <2 x float> %call187, <2 x float>* %169, align 4
  %coerce188.realp = getelementptr inbounds { float, float }, { float, float }* %coerce188, i32 0, i32 0
  %coerce188.real = load float, float* %coerce188.realp, align 4
  %coerce188.imagp = getelementptr inbounds { float, float }, { float, float }* %coerce188, i32 0, i32 1
  %coerce188.imag = load float, float* %coerce188.imagp, align 4
  %conv189 = fptoui float %coerce188.real to i16
  store i16 %conv189, i16* %atomic-temp184, align 2
  %170 = load i16, i16* %atomic-temp184, align 2
  %171 = cmpxchg i16* @usx, i16 %168, i16 %170 monotonic monotonic
  %172 = extractvalue { i16, i1 } %171, 0
  %173 = extractvalue { i16, i1 } %171, 1
  br i1 %173, label %atomic_exit190, label %atomic_cont183

atomic_exit190:                                   ; preds = %atomic_cont183
  store i16 %conv189, i16* @usv, align 2
  %cdv.real191 = load double, double* getelementptr inbounds ({ double, double }, { double, double }* @cdv, i32 0, i32 0), align 8
  %cdv.imag192 = load double, double* getelementptr inbounds ({ double, double }, { double, double }* @cdv, i32 0, i32 1), align 8
  %atomic-load193 = load atomic i64, i64* @llx monotonic, align 8
  br label %atomic_cont194

atomic_cont194:                                   ; preds = %atomic_cont194, %atomic_exit190
  %174 = phi i64 [ %atomic-load193, %atomic_exit190 ], [ %177, %atomic_cont194 ]
  %conv196 = sitofp i64 %174 to double
  %add.r197 = fadd double %conv196, %cdv.real191
  %add.i198 = fadd double 0.000000e+00, %cdv.imag192
  %conv199 = fptosi double %add.r197 to i64
  store i64 %conv199, i64* %atomic-temp195, align 8
  %175 = load i64, i64* %atomic-temp195, align 8
  %176 = cmpxchg i64* @llx, i64 %174, i64 %175 monotonic monotonic
  %177 = extractvalue { i64, i1 } %176, 0
  %178 = extractvalue { i64, i1 } %176, 1
  br i1 %178, label %atomic_exit200, label %atomic_cont194

atomic_exit200:                                   ; preds = %atomic_cont194
  store i64 %174, i64* @llv, align 8
  %179 = load i16, i16* @sv, align 2
  %180 = load i8, i8* @bv, align 1
  %tobool201 = trunc i8 %180 to i1
  %conv202 = zext i1 %tobool201 to i32
  %atomic-load203 = load atomic i128, i128* bitcast (<4 x i32>* @int4x to i128*) monotonic, align 16
  br label %atomic_cont204

atomic_cont204:                                   ; preds = %atomic_cont204, %atomic_exit200
  %181 = phi i128 [ %atomic-load203, %atomic_exit200 ], [ %188, %atomic_cont204 ]
  %182 = bitcast <4 x i32>* %atomic-temp205 to i128*
  store i128 %181, i128* %182, align 16
  %183 = bitcast i128 %181 to <4 x i32>
  store <4 x i32> %183, <4 x i32>* %atomic-temp206, align 16
  %184 = load <4 x i32>, <4 x i32>* %atomic-temp206, align 16
  %vecext = extractelement <4 x i32> %184, i16 %179
  %or207 = or i32 %vecext, %conv202
  %185 = load <4 x i32>, <4 x i32>* %atomic-temp205, align 16
  %vecins = insertelement <4 x i32> %185, i32 %or207, i16 %179
  store <4 x i32> %vecins, <4 x i32>* %atomic-temp205, align 16
  %186 = load i128, i128* %182, align 16
  %187 = cmpxchg i128* bitcast (<4 x i32>* @int4x to i128*), i128 %181, i128 %186 monotonic monotonic
  %188 = extractvalue { i128, i1 } %187, 0
  %189 = extractvalue { i128, i1 } %187, 1
  br i1 %189, label %atomic_exit208, label %atomic_cont204

atomic_exit208:                                   ; preds = %atomic_cont204
  store i32 %or207, i32* @iv, align 4
  %190 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load209 = load atomic i32, i32* bitcast (i8* getelementptr (i8, i8* bitcast (%struct.BitFields* @bfx to i8*), i64 4) to i32*) monotonic, align 4
  br label %atomic_cont210

atomic_cont210:                                   ; preds = %atomic_cont210, %atomic_exit208
  %191 = phi i32 [ %atomic-load209, %atomic_exit208 ], [ %194, %atomic_cont210 ]
  store i32 %191, i32* %atomic-temp211, align 4
  store i32 %191, i32* %atomic-temp212, align 4
  %bf.load = load i32, i32* %atomic-temp212, align 4
  %bf.shl = shl i32 %bf.load, 1
  %bf.ashr = ashr i32 %bf.shl, 1
  %conv213 = sitofp i32 %bf.ashr to x86_fp80
  %sub214 = fsub x86_fp80 %conv213, %190
  %conv215 = fptosi x86_fp80 %sub214 to i32
  %bf.load216 = load i32, i32* %atomic-temp211, align 4
  %bf.value = and i32 %conv215, 2147483647
  %bf.clear = and i32 %bf.load216, -2147483648
  %bf.set = or i32 %bf.clear, %bf.value
  store i32 %bf.set, i32* %atomic-temp211, align 4
  %192 = load i32, i32* %atomic-temp211, align 4
  %193 = cmpxchg i32* bitcast (i8* getelementptr (i8, i8* bitcast (%struct.BitFields* @bfx to i8*), i64 4) to i32*), i32 %191, i32 %192 monotonic monotonic
  %194 = extractvalue { i32, i1 } %193, 0
  %195 = extractvalue { i32, i1 } %193, 1
  br i1 %195, label %atomic_exit217, label %atomic_cont210

atomic_exit217:                                   ; preds = %atomic_cont210
  store i32 %conv215, i32* @iv, align 4
  %196 = load x86_fp80, x86_fp80* @ldv, align 16
  %197 = bitcast i32* %atomic-temp218 to i8*
  invoke void @__atomic_load(i64 4, i8* getelementptr (i8, i8* bitcast (%struct.BitFields_packed* @bfx_packed to i8*), i64 4), i8* %197, i32 0)
          to label %invoke.cont219 unwind label %terminate.lpad

invoke.cont219:                                   ; preds = %atomic_exit217
  br label %atomic_cont220

atomic_cont220:                                   ; preds = %invoke.cont233, %invoke.cont219
  %198 = load i32, i32* %atomic-temp218, align 1
  store i32 %198, i32* %atomic-temp221, align 1
  %199 = load i32, i32* %atomic-temp218, align 1
  store i32 %199, i32* %atomic-temp222, align 4
  %bf.load223 = load i32, i32* %atomic-temp222, align 1
  %bf.shl224 = shl i32 %bf.load223, 1
  %bf.ashr225 = ashr i32 %bf.shl224, 1
  %conv226 = sitofp i32 %bf.ashr225 to x86_fp80
  %mul227 = fmul x86_fp80 %conv226, %196
  %conv228 = fptosi x86_fp80 %mul227 to i32
  %bf.load229 = load i32, i32* %atomic-temp221, align 1
  %bf.value230 = and i32 %conv228, 2147483647
  %bf.clear231 = and i32 %bf.load229, -2147483648
  %bf.set232 = or i32 %bf.clear231, %bf.value230
  store i32 %bf.set232, i32* %atomic-temp221, align 1
  %200 = bitcast i32* %atomic-temp218 to i8*
  %201 = bitcast i32* %atomic-temp221 to i8*
  %call234 = invoke zeroext i1 @__atomic_compare_exchange(i64 4, i8* getelementptr (i8, i8* bitcast (%struct.BitFields_packed* @bfx_packed to i8*), i64 4), i8* %200, i8* %201, i32 0, i32 0)
          to label %invoke.cont233 unwind label %terminate.lpad

invoke.cont233:                                   ; preds = %atomic_cont220
  br i1 %call234, label %atomic_exit235, label %atomic_cont220

atomic_exit235:                                   ; preds = %invoke.cont233
  store i32 %bf.ashr225, i32* @iv, align 4
  %202 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load236 = load atomic i32, i32* getelementptr inbounds (%struct.BitFields2, %struct.BitFields2* @bfx2, i32 0, i32 0) monotonic, align 4
  br label %atomic_cont237

atomic_cont237:                                   ; preds = %atomic_cont237, %atomic_exit235
  %203 = phi i32 [ %atomic-load236, %atomic_exit235 ], [ %206, %atomic_cont237 ]
  store i32 %203, i32* %atomic-temp238, align 4
  store i32 %203, i32* %atomic-temp239, align 4
  %bf.load240 = load i32, i32* %atomic-temp239, align 4
  %bf.ashr241 = ashr i32 %bf.load240, 31
  %conv242 = sitofp i32 %bf.ashr241 to x86_fp80
  %sub243 = fsub x86_fp80 %conv242, %202
  %conv244 = fptosi x86_fp80 %sub243 to i32
  %bf.load245 = load i32, i32* %atomic-temp238, align 4
  %bf.value246 = and i32 %conv244, 1
  %bf.shl247 = shl i32 %bf.value246, 31
  %bf.clear248 = and i32 %bf.load245, 2147483647
  %bf.set249 = or i32 %bf.clear248, %bf.shl247
  store i32 %bf.set249, i32* %atomic-temp238, align 4
  %204 = load i32, i32* %atomic-temp238, align 4
  %205 = cmpxchg i32* getelementptr inbounds (%struct.BitFields2, %struct.BitFields2* @bfx2, i32 0, i32 0), i32 %203, i32 %204 monotonic monotonic
  %206 = extractvalue { i32, i1 } %205, 0
  %207 = extractvalue { i32, i1 } %205, 1
  br i1 %207, label %atomic_exit250, label %atomic_cont237

atomic_exit250:                                   ; preds = %atomic_cont237
  store i32 %conv244, i32* @iv, align 4
  %208 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load251 = load atomic i8, i8* getelementptr (i8, i8* bitcast (%struct.BitFields2_packed* @bfx2_packed to i8*), i64 3) monotonic, align 1
  br label %atomic_cont252

atomic_cont252:                                   ; preds = %atomic_cont252, %atomic_exit250
  %209 = phi i8 [ %atomic-load251, %atomic_exit250 ], [ %215, %atomic_cont252 ]
  %210 = bitcast i32* %atomic-temp253 to i8*
  store i8 %209, i8* %210, align 1
  %211 = bitcast i32* %atomic-temp254 to i8*
  store i8 %209, i8* %211, align 1
  %bf.load255 = load i8, i8* %211, align 1
  %bf.ashr256 = ashr i8 %bf.load255, 7
  %bf.cast = sext i8 %bf.ashr256 to i32
  %conv257 = sitofp i32 %bf.cast to x86_fp80
  %div258 = fdiv x86_fp80 %208, %conv257
  %conv259 = fptosi x86_fp80 %div258 to i32
  %212 = trunc i32 %conv259 to i8
  %bf.load260 = load i8, i8* %210, align 1
  %bf.value261 = and i8 %212, 1
  %bf.shl262 = shl i8 %bf.value261, 7
  %bf.clear263 = and i8 %bf.load260, 127
  %bf.set264 = or i8 %bf.clear263, %bf.shl262
  store i8 %bf.set264, i8* %210, align 1
  %213 = load i8, i8* %210, align 1
  %214 = cmpxchg i8* getelementptr (i8, i8* bitcast (%struct.BitFields2_packed* @bfx2_packed to i8*), i64 3), i8 %209, i8 %213 monotonic monotonic
  %215 = extractvalue { i8, i1 } %214, 0
  %216 = extractvalue { i8, i1 } %214, 1
  br i1 %216, label %atomic_exit265, label %atomic_cont252

atomic_exit265:                                   ; preds = %atomic_cont252
  store i32 %conv259, i32* @iv, align 4
  %217 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load266 = load atomic i32, i32* getelementptr inbounds (%struct.BitFields3, %struct.BitFields3* @bfx3, i32 0, i32 0) monotonic, align 4
  br label %atomic_cont267

atomic_cont267:                                   ; preds = %atomic_cont267, %atomic_exit265
  %218 = phi i32 [ %atomic-load266, %atomic_exit265 ], [ %221, %atomic_cont267 ]
  store i32 %218, i32* %atomic-temp268, align 4
  store i32 %218, i32* %atomic-temp269, align 4
  %bf.load270 = load i32, i32* %atomic-temp269, align 4
  %bf.shl271 = shl i32 %bf.load270, 7
  %bf.ashr272 = ashr i32 %bf.shl271, 18
  %conv273 = sitofp i32 %bf.ashr272 to x86_fp80
  %div274 = fdiv x86_fp80 %conv273, %217
  %conv275 = fptosi x86_fp80 %div274 to i32
  %bf.load276 = load i32, i32* %atomic-temp268, align 4
  %bf.value277 = and i32 %conv275, 16383
  %bf.shl278 = shl i32 %bf.value277, 11
  %bf.clear279 = and i32 %bf.load276, -33552385
  %bf.set280 = or i32 %bf.clear279, %bf.shl278
  store i32 %bf.set280, i32* %atomic-temp268, align 4
  %219 = load i32, i32* %atomic-temp268, align 4
  %220 = cmpxchg i32* getelementptr inbounds (%struct.BitFields3, %struct.BitFields3* @bfx3, i32 0, i32 0), i32 %218, i32 %219 monotonic monotonic
  %221 = extractvalue { i32, i1 } %220, 0
  %222 = extractvalue { i32, i1 } %220, 1
  br i1 %222, label %atomic_exit281, label %atomic_cont267

atomic_exit281:                                   ; preds = %atomic_cont267
  store i32 %bf.ashr272, i32* @iv, align 4
  %223 = load x86_fp80, x86_fp80* @ldv, align 16
  %224 = bitcast i32* %atomic-temp282 to i24*
  %225 = bitcast i24* %224 to i8*
  invoke void @__atomic_load(i64 3, i8* getelementptr (i8, i8* bitcast (%struct.BitFields3_packed* @bfx3_packed to i8*), i64 1), i8* %225, i32 0)
          to label %invoke.cont283 unwind label %terminate.lpad

invoke.cont283:                                   ; preds = %atomic_exit281
  br label %atomic_cont284

atomic_cont284:                                   ; preds = %invoke.cont299, %invoke.cont283
  %226 = bitcast i32* %atomic-temp285 to i24*
  %227 = load i24, i24* %224, align 1
  store i24 %227, i24* %226, align 1
  %228 = load i24, i24* %224, align 1
  %229 = bitcast i32* %atomic-temp286 to i24*
  store i24 %228, i24* %229, align 1
  %bf.load287 = load i24, i24* %229, align 1
  %bf.shl288 = shl i24 %bf.load287, 7
  %bf.ashr289 = ashr i24 %bf.shl288, 10
  %bf.cast290 = sext i24 %bf.ashr289 to i32
  %conv291 = sitofp i32 %bf.cast290 to x86_fp80
  %add292 = fadd x86_fp80 %conv291, %223
  %conv293 = fptosi x86_fp80 %add292 to i32
  %230 = trunc i32 %conv293 to i24
  %bf.load294 = load i24, i24* %226, align 1
  %bf.value295 = and i24 %230, 16383
  %bf.shl296 = shl i24 %bf.value295, 3
  %bf.clear297 = and i24 %bf.load294, -131065
  %bf.set298 = or i24 %bf.clear297, %bf.shl296
  store i24 %bf.set298, i24* %226, align 1
  %231 = bitcast i24* %224 to i8*
  %232 = bitcast i24* %226 to i8*
  %call300 = invoke zeroext i1 @__atomic_compare_exchange(i64 3, i8* getelementptr (i8, i8* bitcast (%struct.BitFields3_packed* @bfx3_packed to i8*), i64 1), i8* %231, i8* %232, i32 0, i32 0)
          to label %invoke.cont299 unwind label %terminate.lpad

invoke.cont299:                                   ; preds = %atomic_cont284
  br i1 %call300, label %atomic_exit301, label %atomic_cont284

atomic_exit301:                                   ; preds = %invoke.cont299
  store i32 %conv293, i32* @iv, align 4
  %233 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load302 = load atomic i64, i64* bitcast (%struct.BitFields4* @bfx4 to i64*) monotonic, align 8
  br label %atomic_cont303

atomic_cont303:                                   ; preds = %atomic_cont303, %atomic_exit301
  %234 = phi i64 [ %atomic-load302, %atomic_exit301 ], [ %238, %atomic_cont303 ]
  store i64 %234, i64* %atomic-temp304, align 8
  store i64 %234, i64* %atomic-temp305, align 8
  %bf.load306 = load i64, i64* %atomic-temp305, align 8
  %bf.shl307 = shl i64 %bf.load306, 47
  %bf.ashr308 = ashr i64 %bf.shl307, 63
  %bf.cast309 = trunc i64 %bf.ashr308 to i32
  %conv310 = sitofp i32 %bf.cast309 to x86_fp80
  %mul311 = fmul x86_fp80 %conv310, %233
  %conv312 = fptosi x86_fp80 %mul311 to i32
  %235 = zext i32 %conv312 to i64
  %bf.load313 = load i64, i64* %atomic-temp304, align 8
  %bf.value314 = and i64 %235, 1
  %bf.shl315 = shl i64 %bf.value314, 16
  %bf.clear316 = and i64 %bf.load313, -65537
  %bf.set317 = or i64 %bf.clear316, %bf.shl315
  store i64 %bf.set317, i64* %atomic-temp304, align 8
  %236 = load i64, i64* %atomic-temp304, align 8
  %237 = cmpxchg i64* bitcast (%struct.BitFields4* @bfx4 to i64*), i64 %234, i64 %236 monotonic monotonic
  %238 = extractvalue { i64, i1 } %237, 0
  %239 = extractvalue { i64, i1 } %237, 1
  br i1 %239, label %atomic_exit318, label %atomic_cont303

atomic_exit318:                                   ; preds = %atomic_cont303
  store i32 %conv312, i32* @iv, align 4
  %240 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load319 = load atomic i8, i8* getelementptr inbounds (%struct.BitFields4_packed, %struct.BitFields4_packed* @bfx4_packed, i32 0, i32 0, i64 2) monotonic, align 1
  br label %atomic_cont320

atomic_cont320:                                   ; preds = %atomic_cont320, %atomic_exit318
  %241 = phi i8 [ %atomic-load319, %atomic_exit318 ], [ %247, %atomic_cont320 ]
  %242 = bitcast i32* %atomic-temp321 to i8*
  store i8 %241, i8* %242, align 1
  %243 = bitcast i32* %atomic-temp322 to i8*
  store i8 %241, i8* %243, align 1
  %bf.load323 = load i8, i8* %243, align 1
  %bf.shl324 = shl i8 %bf.load323, 7
  %bf.ashr325 = ashr i8 %bf.shl324, 7
  %bf.cast326 = sext i8 %bf.ashr325 to i32
  %conv327 = sitofp i32 %bf.cast326 to x86_fp80
  %sub328 = fsub x86_fp80 %conv327, %240
  %conv329 = fptosi x86_fp80 %sub328 to i32
  %244 = trunc i32 %conv329 to i8
  %bf.load330 = load i8, i8* %242, align 1
  %bf.value331 = and i8 %244, 1
  %bf.clear332 = and i8 %bf.load330, -2
  %bf.set333 = or i8 %bf.clear332, %bf.value331
  store i8 %bf.set333, i8* %242, align 1
  %245 = load i8, i8* %242, align 1
  %246 = cmpxchg i8* getelementptr inbounds (%struct.BitFields4_packed, %struct.BitFields4_packed* @bfx4_packed, i32 0, i32 0, i64 2), i8 %241, i8 %245 monotonic monotonic
  %247 = extractvalue { i8, i1 } %246, 0
  %248 = extractvalue { i8, i1 } %246, 1
  br i1 %248, label %atomic_exit334, label %atomic_cont320

atomic_exit334:                                   ; preds = %atomic_cont320
  store i32 %bf.cast326, i32* @iv, align 4
  %249 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load335 = load atomic i64, i64* bitcast (%struct.BitFields4* @bfx4 to i64*) monotonic, align 8
  br label %atomic_cont336

atomic_cont336:                                   ; preds = %atomic_cont336, %atomic_exit334
  %250 = phi i64 [ %atomic-load335, %atomic_exit334 ], [ %253, %atomic_cont336 ]
  store i64 %250, i64* %atomic-temp337, align 8
  store i64 %250, i64* %atomic-temp338, align 8
  %bf.load339 = load i64, i64* %atomic-temp338, align 8
  %bf.shl340 = shl i64 %bf.load339, 40
  %bf.ashr341 = ashr i64 %bf.shl340, 57
  %conv342 = sitofp i64 %bf.ashr341 to x86_fp80
  %div343 = fdiv x86_fp80 %conv342, %249
  %conv344 = fptosi x86_fp80 %div343 to i64
  %bf.load345 = load i64, i64* %atomic-temp337, align 8
  %bf.value346 = and i64 %conv344, 127
  %bf.shl347 = shl i64 %bf.value346, 17
  %bf.clear348 = and i64 %bf.load345, -16646145
  %bf.set349 = or i64 %bf.clear348, %bf.shl347
  store i64 %bf.set349, i64* %atomic-temp337, align 8
  %251 = load i64, i64* %atomic-temp337, align 8
  %252 = cmpxchg i64* bitcast (%struct.BitFields4* @bfx4 to i64*), i64 %250, i64 %251 monotonic monotonic
  %253 = extractvalue { i64, i1 } %252, 0
  %254 = extractvalue { i64, i1 } %252, 1
  br i1 %254, label %atomic_exit350, label %atomic_cont336

atomic_exit350:                                   ; preds = %atomic_cont336
  %conv351 = trunc i64 %conv344 to i32
  store i32 %conv351, i32* @iv, align 4
  %255 = load x86_fp80, x86_fp80* @ldv, align 16
  %atomic-load352 = load atomic i8, i8* getelementptr inbounds (%struct.BitFields4_packed, %struct.BitFields4_packed* @bfx4_packed, i32 0, i32 0, i64 2) monotonic, align 1
  br label %atomic_cont353

atomic_cont353:                                   ; preds = %atomic_cont353, %atomic_exit350
  %256 = phi i8 [ %atomic-load352, %atomic_exit350 ], [ %262, %atomic_cont353 ]
  %257 = bitcast i64* %atomic-temp354 to i8*
  store i8 %256, i8* %257, align 1
  %258 = bitcast i64* %atomic-temp355 to i8*
  store i8 %256, i8* %258, align 1
  %bf.load356 = load i8, i8* %258, align 1
  %bf.ashr357 = ashr i8 %bf.load356, 1
  %bf.cast358 = sext i8 %bf.ashr357 to i64
  %conv359 = sitofp i64 %bf.cast358 to x86_fp80
  %add360 = fadd x86_fp80 %conv359, %255
  %conv361 = fptosi x86_fp80 %add360 to i64
  %259 = trunc i64 %conv361 to i8
  %bf.load362 = load i8, i8* %257, align 1
  %bf.value363 = and i8 %259, 127
  %bf.shl364 = shl i8 %bf.value363, 1
  %bf.clear365 = and i8 %bf.load362, 1
  %bf.set366 = or i8 %bf.clear365, %bf.shl364
  store i8 %bf.set366, i8* %257, align 1
  %260 = load i8, i8* %257, align 1
  %261 = cmpxchg i8* getelementptr inbounds (%struct.BitFields4_packed, %struct.BitFields4_packed* @bfx4_packed, i32 0, i32 0, i64 2), i8 %256, i8 %260 monotonic monotonic
  %262 = extractvalue { i8, i1 } %261, 0
  %263 = extractvalue { i8, i1 } %261, 1
  br i1 %263, label %atomic_exit367, label %atomic_cont353

atomic_exit367:                                   ; preds = %atomic_cont353
  %conv368 = trunc i64 %conv361 to i32
  store i32 %conv368, i32* @iv, align 4
  %264 = load i64, i64* @ulv, align 8
  %conv369 = uitofp i64 %264 to float
  %atomic-load370 = load atomic i64, i64* bitcast (<2 x float>* @float2x to i64*) monotonic, align 8
  br label %atomic_cont371

atomic_cont371:                                   ; preds = %atomic_cont371, %atomic_exit367
  %265 = phi i64 [ %atomic-load370, %atomic_exit367 ], [ %274, %atomic_cont371 ]
  %266 = bitcast <2 x float>* %atomic-temp372 to i64*
  store i64 %265, i64* %266, align 8
  %267 = bitcast i64 %265 to <2 x float>
  store <2 x float> %267, <2 x float>* %atomic-temp373, align 8
  %268 = load <2 x float>, <2 x float>* %atomic-temp373, align 8
  %269 = extractelement <2 x float> %268, i64 0
  %sub374 = fsub float %conv369, %269
  %270 = load <2 x float>, <2 x float>* %atomic-temp372, align 8
  %271 = insertelement <2 x float> %270, float %sub374, i64 0
  store <2 x float> %271, <2 x float>* %atomic-temp372, align 8
  %272 = load i64, i64* %266, align 8
  %273 = cmpxchg i64* bitcast (<2 x float>* @float2x to i64*), i64 %265, i64 %272 monotonic monotonic
  %274 = extractvalue { i64, i1 } %273, 0
  %275 = extractvalue { i64, i1 } %273, 1
  br i1 %275, label %atomic_exit375, label %atomic_cont371

atomic_exit375:                                   ; preds = %atomic_cont371
  store float %269, float* @fv, align 4
  %276 = load double, double* @dv, align 8
  %277 = call i32 @llvm.read_register.i32(metadata !0)
  %conv376 = sitofp i32 %277 to double
  %div377 = fdiv double %276, %conv376
  %conv378 = fptosi double %div377 to i32
  call void @llvm.write_register.i32(metadata !0, i32 %conv378)
  store i32 %conv378, i32* @iv, align 4
  call void @__kmpc_flush(%ident_t* @0)
  %278 = atomicrmw xchg i32* @ix, i32 5 monotonic
  call void @llvm.write_register.i32(metadata !0, i32 %278)
  ret i32 0

terminate.lpad:                                   ; preds = %atomic_cont284, %atomic_exit281, %atomic_cont220, %atomic_exit217, %atomic_cont136, %atomic_exit133, %atomic_cont104, %atomic_exit99, %atomic_cont61, %atomic_exit56, %atomic_cont52, %atomic_exit47, %atomic_cont44, %atomic_exit41
  %279 = landingpad { i8*, i32 }
          catch i8* null
  %280 = extractvalue { i8*, i32 } %279, 0
  call void @__clang_call_terminate(i8* %280) #4
  unreachable
}

declare void @__atomic_load(i64, i8*, i8*, i32)

declare i32 @__gxx_personality_v0(...)

; Function Attrs: noinline noreturn nounwind
define linkonce_odr hidden void @__clang_call_terminate(i8*) #1 comdat {
  %2 = call i8* @__cxa_begin_catch(i8* %0) #3
  call void @_ZSt9terminatev() #4
  unreachable
}

declare i8* @__cxa_begin_catch(i8*)

declare void @_ZSt9terminatev()

declare i1 @__atomic_compare_exchange(i64, i8*, i8*, i8*, i32, i32)

declare void @__kmpc_flush(%ident_t*)

declare <2 x float> @__divsc3(float, float, float, float)

; Function Attrs: nounwind readonly
declare i32 @llvm.read_register.i32(metadata) #2

; Function Attrs: nounwind
declare void @llvm.write_register.i32(metadata, i32) #3

attributes #0 = { norecurse uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline noreturn nounwind }
attributes #2 = { nounwind readonly }
attributes #3 = { nounwind }
attributes #4 = { noreturn nounwind }

!llvm.named.register.esp = !{!0}
!llvm.ident = !{!1}

!0 = !{!"esp"}
!1 = !{!"clang version 3.9.0 "}
