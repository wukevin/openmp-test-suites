
#include <omp.h>
#include <iostream>

#define N 1024
#define M 1024

void init(float Q[][N], const int dim){
  for(int j = 0; j < dim; j++)
    for(int i = 0; i < dim; i++)
      Q[j][i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}

#pragma omp declare target
float Q[N][N];
#pragma omp declare simd uniform(i) linear(k) notinbranch
float P(const int i, int k){
  return Q[i][k] * Q[k][i];
}
#pragma omp end declare target

float accumOMP()
{
  float tmp = 0.0;
  int i, k;
  #pragma omp target
  #pragma omp parallel for reduction(+:tmp)
  for (i=0; i < N; i++) {
    float tmp1 = 0.0;
    #pragma omp simd reduction(+:tmp1)
    for (k=0; k < M; k++) {
      tmp1 += P(i,k);
    }
    tmp += tmp1;
  }
  return tmp;
}

float accum()
{
  float tmp = 0.0;
  int i, k;
  for (i=0; i < N; i++) {
    float tmp1 = 0.0;
    for (k=0; k < M; k++) {
      tmp1 += Q[i][k] * Q[k][i];
    }
    tmp += tmp1;
  }
  return tmp;
}

int main()
{

  //Initialize test data

  srand (static_cast <unsigned> (time(0)));
  init(Q, N);

  //Execute the kernel function
  float res = accumOMP();
  float ans = accum();

  //Check the result
  bool ret = true;
  if(res - ans > 0.01f)
    ret = false;

  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}

