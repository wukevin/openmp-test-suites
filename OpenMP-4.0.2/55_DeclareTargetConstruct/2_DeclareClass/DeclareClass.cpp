
#include <omp.h>
#include <iostream>

#define THRESHOLD 1000000

struct typeX{
  int a;
};

class typeY{
  int a;
  public:
    int foo() {return a^0x01;}
};
#pragma omp declare target
struct typeX varX; //ok
class typeY varY; //ok if varY.foo() not called on target device
#pragma omp end declare target

void foo()
{
  #pragma omp target
  {
    varX.a = 100;  //ok
    varY.foo(); //error foo() is not available on a target device
  }
}

int main()
{
  //Execute the kernel function
  foo();

  //Check the result
  bool ret = true;
  if(varX.a != 100)
    ret = false;

  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}

