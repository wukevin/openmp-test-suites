
#include <omp.h>
#include <iostream>

#define CHUNKSZ 64

void init(float *v1, float *v2, int N){
  for (int i = 0; i < N; ++i) {
    v1[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    v2[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
  }
}

void vecAdd(float *p, float *v1, float *v2, int N)
{
  int i, C;
  for (C = 0; C<N; C+=CHUNKSZ)
  {
    #pragma omp task
    #pragma omp target map(to:v1[C:C+CHUNKSZ], v2[C:C+CHUNKSZ]) map(from:p[C:C+CHUNKSZ])
    #pragma omp parallel for 
    for (i=C; i<C+CHUNKSZ; i++)
      p[i] = v1[i] + v2[i];
  }
  #pragma omp taskwait
}

int main()
{
  int N = 1024;
  //Initialize test data
  float p[N], v1[N], v2[N];
  float ans[N];

  srand (static_cast <unsigned> (time(0)));
  init(v1, v2, N);
  for (int i = 0; i < N; ++i) 
    ans[i] = v1[i] + v2[i];

  //Execute the kernel function
  vecAdd(v1, v2, p, N);

  //Check the result
  bool ret = true;
  for(int i = 0; i < N; ++i) {
    if(p[i] - ans[i] > 0.01f){
      ret = false;
      break;
    }
  }

  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}

