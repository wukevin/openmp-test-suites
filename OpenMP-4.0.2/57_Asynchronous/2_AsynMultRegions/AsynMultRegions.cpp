
#include <omp.h>
#include <iostream>

#define CHUNKSZ 64

void foo(){
  std::cout << "Ascyrhonous Print\n";
}

void init(float *v1, float *v2, int N){
  for (int i = 0; i < N; ++i) {
    v1[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    v2[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
  }
}

void vecAdd(float *v1, float *v2, float *p, int N, int dev)
{
  int i;
  #pragma omp task shared(v1, v2) depend(out: v1, v2)
  #pragma omp target map(v1, v2)
  {
    // check whether on device dev
    if (omp_is_initial_device())
      abort();
    v1 = (float*)malloc(N*sizeof(float));
    v2 = (float*)malloc(N*sizeof(float));
    init(v1, v2, N);
  }
  foo(); // execute other work asychronously
  #pragma omp task shared(v1, v2, p) depend(in: v1, v2)
  #pragma omp target map(to: v1, v2) map(from: p[0:N])
  {
    // check whether on device dev
    if (omp_is_initial_device())
      abort();
    #pragma omp parallel for
    for (i=0; i<N; i++)
      p[i] = v1[i] + v2[i];
  }
  #pragma omp taskwait
}

int main()
{
  int N = 1024;
  //Initialize test data
  float p[N], *v1, *v2;
  float ans[N];

  srand (static_cast <unsigned> (time(0)));

  //Execute the kernel function
  vecAdd(v1, v2, p, N, 0);

  //Caculate golden ans after vecAdd(in charge of malloc and init)
  for (int i = 0; i < N; ++i) 
    ans[i] = v1[i] + v2[i];

  //Check the result
  bool ret = true;
  for(int i = 0; i < N; ++i) {
    if(p[i] - ans[i] > 0.01f){
      ret = false;
      break;
    }
  }

  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}

