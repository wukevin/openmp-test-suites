
#include <omp.h>
#include "stdio.h"
#include <iostream>
#include <cmath>

#define ROWS 32
#define COLS 32

void init(float Q[][COLS], float ans[][COLS], const int rows){
  for(int j = 0; j < ROWS; j++)
    for (int i = 0; i < COLS; i++) {
      Q[j][i] = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX))*1e3;
      ans[j][i] = Q[j][i];
    }
}

void gradSchmidt(float Q[][COLS], const int rows)
{
  int cols = COLS;
  #pragma omp target data map(Q[0:rows][0:cols])
  for(int i=0; i < cols; i++)
  {
    float tmp = 0.0;
    #pragma omp target
    #pragma omp parallel for reduction(+:tmp)
    for(int j=0; j < rows; j++)
      tmp += (Q[j][i] * Q[j][i]);
    tmp = 1/sqrt(tmp);
    #pragma omp target
    #pragma omp parallel for
    for(int j=0; j < rows; j++)
      Q[j][i] *= tmp;
  }
}

int main()
{

   //Initialize test data
  float Q[ROWS][COLS];
  float ans[ROWS][COLS];

  srand (static_cast <unsigned> (time(0)));
  init(Q, ans, ROWS);
  for(int i = 0; i < COLS; i++){
    float tmp = 0.0;
    for(int j = 0; j < ROWS; j++)
      tmp += (ans[j][i] * ans[j][i]);
    tmp = 1/sqrt(tmp);
    for(int j=0; j < ROWS; j++)
      ans[j][i] *= tmp;
  }

  //Execute the kernel function
  gradSchmidt(Q, ROWS);

  //Check the result
  bool ret = true;
  for(int j = 0; j < ROWS; j++){
    for(int i = 0; i < COLS; ++i) {
      if(Q[j][i] - ans[j][i] > 0.01f){
        ret = false;
        break;
      }
    }
    if(!ret)
      break;
  }

  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}

