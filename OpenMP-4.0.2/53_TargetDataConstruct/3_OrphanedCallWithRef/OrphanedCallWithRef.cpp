
#include <omp.h>
#include <iostream>

void init(float *v1, float *v2, int N){
  for (int i = 0; i < N; ++i) {
    v1[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    v2[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
  }
}

void vecAdd(float* &, float* &, float* &, int &);

void foo(float *v1, float *v2, float *p0, int N){
  #pragma omp target data map(to: v1[0:N], v2[:N]) map(from: p0[0:N])
  {
    vecAdd(v1, v2, p0, N);
  }
}

void vecAdd(float* &v3, float* &v4, float* &p1, int &N){
  int i;
  #pragma omp target map(to: v3[0:N], v4[:N]) map(from: p1[0:N])
  #pragma omp parallel for
  for (i=0; i<N; i++)
    p1[i] = v3[i] + v4[i];
}


int main()
{
  int N = 1024;

  //Initialize test data
  float p[N], v1[N], v2[N];
  float ans[N];

  srand (static_cast <unsigned> (time(0)));
  init(v1, v2, N);
  for(int i = 0; i < N; i++)
    ans[i] = v1[i] + v2[i];

  //Execute the kernel function
  foo(v1, v2, p, N);

  //Check the result
  bool ret = true;
  for(int i = 0; i < N; ++i) {
    if(p[i] - ans[i] > 0.01f){
      ret = false;
      break;
    }
  }

  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}

