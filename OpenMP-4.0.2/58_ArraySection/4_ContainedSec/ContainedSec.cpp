
#include"omp.h"
#include <iostream>

int A[30];

void foo(){
  int *p;
  #pragma omp target data map(A[0:10])
  {
    p = &A[0];
    #pragma omp target map(p[3:7])
    {
      A[2] = 12345;
      p[8] = 12345;
      A[8] = 54321;
    }
  }
}

int main(){
  void foo();
  bool ret = true;
  if(A[2] != 12345 && A[8] != 54321)
    ret = false;
  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}
