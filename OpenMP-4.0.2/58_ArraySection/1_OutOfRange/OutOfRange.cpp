
#include"omp.h"
#include <iostream>

void foo(int *A){
  int *p;
  #pragma omp target data map(A[0:4])
  {
    /* Cannot map distinct parts of the same array */
    #pragma omp target map(A[7:20])
    {
      A[2] = 12345;
      p[8] = 54321;
    }
  }
}

int main(){
  int A[30];
  void foo();
  bool ret = true;
  if(A[8] == 54321)
    ret = false;
  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}
