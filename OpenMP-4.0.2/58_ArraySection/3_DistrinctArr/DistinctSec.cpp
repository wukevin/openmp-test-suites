
#include"omp.h"
#include <iostream>

int A[30];

void foo(){
  int *p;
  #pragma omp target data map(A[0:4])
  {
    p = &A[0];
    #pragma omp target map(p[7:20])
    {
      A[2] = 12345;
      p[8] = 54321;
    }
  }
}

int main(){
  bool ret = true;
  if(A[2] != 12345 && A[8] != 54321)
    ret = false;
  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}
