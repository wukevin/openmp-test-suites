
#include"omp.h"
#include <iostream>

void foo(int *A){
  int *p;
  #pragma omp target data map(A[0:4])
  {
    p = &A[0];
    /* invalid because p[3] and A[3] are the same
    * location on the host but the array section
    * specified via p[...] is not a subset of A[0:4] */
    #pragma omp target map(p[3:20])
    {
      A[2] = 12345;
      p[8] = 54321;
    }
  }
}

int main(){
  int A[30];
  void foo();
  bool ret = true;
  if(A[8] == 54321)
    ret = false;
  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}
