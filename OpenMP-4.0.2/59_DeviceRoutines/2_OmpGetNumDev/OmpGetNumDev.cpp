
#include <omp.h>
#include <iostream>

void init(float *v1, float *v2, int N){
  for (int i = 0; i < N; ++i) {
    v1[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    v2[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
  }
}


void vecMult(float *v1, float *v2, float *p, int N){
  int i;
  int nthreads;
  int nDev = omp_get_num_devices();
  bool doOffload = (nDev>0 && N>512);
  if(doOffload){
    std::cout << "Execute on target device\n" \
             << "Number of devices" << nDev << "\n";
  }
  else
    std::cout << "Execute on initial device\n";
  #pragma omp target if(doOffload) map(to:v1[0:N], v2[0:N]) map(from:p[0:N])
  #pragma omp parallel for if(N>256) private(i) 
  for (i=0; i<N; i++)
    p[i] = v1[i] * v2[i];
}

int main()
{
  int N = 1024;
  //Initialize test data
  float p[N], v1[N], v2[N];
  float ans[N];

  srand (static_cast <unsigned> (time(0)));
  init(v1, v2, N);
  for (int i = 0; i < N; ++i) 
    ans[i] = v1[i] * v2[i];

  //Execute the kernel function
  vecMult(v1, v2, p, N);

  //Check the result
  bool ret = true;
  for(int i = 0; i < N; ++i) {
    if(p[i] - ans[i] > 0.01f){
      ret = false;
      break;
    }
  }

  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}

