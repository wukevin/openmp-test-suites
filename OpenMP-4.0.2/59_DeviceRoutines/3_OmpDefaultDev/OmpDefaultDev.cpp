
#include <omp.h>
#include <iostream>

int main()
{
    int default_device = omp_get_default_device();
    std::cout << "Default device = "<< default_device << "\n";
    omp_set_default_device(default_device+1);
    if (omp_get_default_device() != default_device+1)
      std::cout << "Default device is still = " << default_device << "\n";
}

