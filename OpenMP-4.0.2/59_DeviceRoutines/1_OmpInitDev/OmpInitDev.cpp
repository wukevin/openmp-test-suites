
#include <omp.h>
#include <iostream>

void init(float *v1, float *v2, int N){
  for (int i = 0; i < N; ++i) {
    v1[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    v2[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
  }
}

#pragma omp declare target
void vecMult(float *v1, float *v2, float *p, int N);
#pragma omp end declare target

void foo(float *v1, float *v2, float *p, int N){
  #pragma omp target device(42) map(p[:N], v1[:N], v2[:N])
  {
    vecMult(v1, v2, p, N);
  }
}

void vecMult(float *v1, float *v2, float *p, int N){
  int i;
  int nthreads;
  if(!omp_is_initial_device()){
    std::cout <<"1024 threads on target device\n";
    nthreads = 1024;
  }else{
    std::cout << "8 threads on initial device\n";
    nthreads = 8;
  }
  #pragma omp parallel for private(i) num_threads(nthreads);
  for (i=0; i<N; i++)
    p[i] = v1[i] * v2[i];
}

int main()
{
  int N = 1024;

  //Initialize test data
  float p[N], v1[N], v2[N];
  float ans[N];

  srand (static_cast <unsigned> (time(0)));
  init(v1, v2, N);
  for (int i = 0; i < N; ++i) 
    ans[i] = v1[i] * v2[i];

  //Execute the kernel function
  foo(v1, v2, p, N);

  //Check the result
  bool ret = true;
  for(int i = 0; i < N; ++i) {
    if(p[i] - ans[i] > 0.01f){
      ret = false;
      break;
    }
  }

  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}

