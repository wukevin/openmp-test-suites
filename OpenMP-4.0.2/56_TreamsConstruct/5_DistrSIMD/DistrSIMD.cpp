
#include <omp.h>
#include <iostream>

void init(float *v1, float *v2, int N){
  for (int i = 0; i < N; ++i) {
    v1[i] = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX))*1e3;
    v2[i] = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX))*1e3;
  }
}

void vecMult(float *p, float *v1, float *v2, int N)
{
  int i;
  #pragma omp target teams map(to: v1[0:N], v2[:N]) map(from: p[0:N])
  #pragma omp distribute simd
  for (i=0; i<N; i++)
    p[i] = v1[i] * v2[i];
}

int main()
{
  int N = 1024;
  //Initialize test data
  float p[N], v1[N], v2[N];
  float ans[N];

  srand (static_cast <unsigned> (time(0)));
  init(v1, v2, N);
  for (int i = 0; i < N; ++i) 
    ans[i] = v1[i] * v2[i];

  //Execute the kernel function
  vecMult(v1, v2, p, N);

  //Check the result
  bool ret = true;
  for(int i = 0; i < N; ++i) {
    if(p[i] - ans[i] > 0.01f){
      ret = false;
      break;
    }
  }

  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}

