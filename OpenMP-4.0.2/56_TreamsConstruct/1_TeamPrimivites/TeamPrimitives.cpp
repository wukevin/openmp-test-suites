
#include <omp.h>
#include <iostream>

void init(float *v1, float *v2, int N){
  for (int i = 0; i < N; ++i) {
    v1[i] = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX))*1e3;
    v2[i] = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX))*1e3;
  }
}

float dotprod(float v1[], float v2[], int N)
{
  float sum0 = 0.0;
  float sum1 = 0.0;
  #pragma omp target map(to: v1[:N], v2[:N])
  #pragma omp teams num_teams(2)
  {
    int i;
    if (omp_get_num_teams() != 2)
      abort();
    if (omp_get_team_num() == 0)
    {
      #pragma omp parallel for reduction(+:sum0)
      for (i=0; i<N/2; i++)
        sum0 += v1[i] * v2[i];
    }
    else if (omp_get_team_num() == 1)
    {
      #pragma omp parallel for reduction(+:sum1)
      for (i=N/2; i<N; i++)
        sum1 += v1[i] * v2[i];
    }
  }
  return sum0 + sum1;
}

int main()
{
  int N = 1024;
  //Initialize test data
  float v1[N], v2[N];
  float ans = 0.0, res;

  srand (static_cast <unsigned> (time(0)));
  init(v1, v2, N);
  for (int i = 0; i < N; ++i) 
    ans += v1[i] * v2[i];

  //Execute the kernel function
  res = dotprod(v1, v2, N);

  //Check the result
  bool ret = true;
  if(res - ans > 0.01f)
    ret = false;

  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}

