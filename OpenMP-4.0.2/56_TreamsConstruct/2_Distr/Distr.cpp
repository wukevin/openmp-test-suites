
#include <omp.h>
#include <iostream>

void init(float *v1, float *v2, int N){
  for (int i = 0; i < N; ++i) {
    v1[i] = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX))*1e3;
    v2[i] = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX))*1e3;
  }
}

float dotprod(float v1[], float v2[], int N, int block_size,
  int num_teams, int block_threads)
{
  float sum = 0;
  int i, i0;
  #pragma omp target map(to: v1[0:N], v2[0:N])
  #pragma omp teams num_teams(num_teams) thread_limit(block_threads) \
    reduction(+:sum)
  #pragma omp distribute 
  for (i0=0; i0<N; i0 += block_size)
    #pragma omp parallel for reduction(+:sum)
    for (i=i0; i< std::min(i0+block_size,N); i++)
      sum += v1[i] * v2[i];
  return sum;
}

int main()
{
  int N = 1024;
  //Initialize test data
  float v1[N], v2[N];
  float ans = 0.0, res;

  srand (static_cast <unsigned> (time(0)));
  init(v1, v2, N);
  for (int i = 0; i < N; ++i) 
    ans += v1[i] * v2[i];

  //Execute the kernel function
  res = dotprod(v1, v2, N, 4, 2, 4);

  //Check the result
  bool ret = true;
  if(res - ans > 0.01f)
    ret = false;

  if(ret)
    std::cout << "Success\n";
  else
    std::cout << "Fail\n";
  return !ret;
}

