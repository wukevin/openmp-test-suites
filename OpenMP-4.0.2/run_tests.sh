#!/bin/bash

set -uo pipefail

wd=$(pwd)
dirlist=$(find . -type d)

error=0
count=0
failed=""

checkerror() {
  if [ $1 -ne 0 ]
  then
    error=$(($error + 1))
    if [ -n "$2" ]
    then
      failed=$failed"\n $2"
    fi
  fi

  count=$(($count + 1))
}

for dir in $dirlist
do
  cd $wd/$dir
  if [ -f Makefile ]; then
    printf "\n****************************************************************\n"
    printf "$dir"
    make
    ./a.out
    checkerror $? "$dir"
  fi
done

printf "\n****************************************************************\n"
printf "failed: $error\n"
printf "tests failed:\n$failed\n"
printf "total: $count\n"

